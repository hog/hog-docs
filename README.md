# Hog Documentation README

Hog documentation is host on readthedocs.org (<https://hog.readthedocs.io/en/stable/>) and it is implemented using the Sphinx documentation generator (<https://www.sphinx-doc.org/en/master/>). The documentation itself it is written in Markdown and compiled using the [MyST plugin](https://myst-parser.readthedocs.io/en/latest/index.html).

## Build the documentation locally

First of all, clone the hog-docs repository

```bash
git clone ssh://git@gitlab.cern.ch:7999/hog/hog-docs.git
cd hog-docs
```

The branch named as the latest release (e.g. 2020.2) represents the latest documentation and is protected. Developer should hence work in `develop` branch, or create MR targeting it.

Checkout the `develop` branch

```bash
git checkout develop
```

To build the documentation, you need to install the python packages specified in `requirements.txt`. (You need python 3.7 and pip installed first)

```bash
pip install -r requirements.txt
```

Then to compile the documentation locally, simply issue 

```bash
make html
```

This will create a build folder. To look at the html documentation, open with your favourite browser the file `build/html/index.html`. If you want to clean the build folder, issue

```bash
make clean
```

## ReadTheDocs

The documentation is stored on readthedocs.org. We setup the server with a webhook that looks only at specific branch/tags. ReadTheDocs can create versioned documentations, creating a version for each selected branch/tags. Versions can be browsed from menu in the bottom right corner. 

Public accessible versions are `stable` (most recent tag) and selected tags.

We generate also documentation for the develop branch, which is uploaded in the hidden webpage (<https://hog.readthedocs.io/en/develop/>).

To look at the compilation status on readthedocs, you need to create an account and tell Davide Cieri (davide.cieri@cern.ch) your readthedocs username. Later you should have access to <https://readthedocs.org/projects/hog/>, where you can configure the readthedocs server and see the compilation logs.
