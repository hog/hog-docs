#!/bin/bash

REALGIT=$(which git)

RETRIES=3
DELAY=2
COUNT=0
while [ $COUNT -lt $RETRIES ]; do
  $REALGIT "$@"
  if [ $? -eq 0 ]; then
    RETRIES=0
    break
  fi
  let COUNT=$COUNT+1
  sleep $DELAY
done
