# Suggest new features
When you suggest a feature, please do it one at the time, opening an issue using the label ![proposal](./figures/proposal.png).

You can do that [at this link](https://gitlab.cern.ch/hog/Hog/issues).

Let us know:

- if it's a missing feature or a modification to an existing feature
- what will this save or fix
- to which part of Hog this is related to

Please do not use any other labels apart form "Feature proposal", "Question" and "Problem report" because they are meant for developers rather than users.
