# Hog: HDL on git

## Introduction

Coordinating firmware development among many international collaborators is becoming a very widespread problem. Guaranteeing firmware synthesis with Place and Route **reproducibility** and assuring **traceability** of binary files is paramount.

Hog tackles these issues by exploiting advanced git features and integrating itself with HDL IDEs: **Xilinx Vivado**, **Xilinx ISE (planAhead)[^ise]** or **Intel Quartus[^quartus]**. The integration with these tools intends to reduce as much as possible useless overhead work for the developers.

[^quartus]:Not all Hog features are supported for Quartus. If a feature described in the documentation is supported only by Xilinx IDEs, it is clearly stated. If some of the functions you are looking for are not cited in the documentation they might be [scheduled for development in a future release](https://gitlab.cern.ch/hog/Hog/-/issues)
[^ise]:Hog provides a minimal support for earlier generations of Xilinx chips using the planAhead tool. The planAhead GUI is very similar to Vivado and should be very familiar to Vivado users. Under the hood it still uses the same tools as ISE but provides a Tcl-scriptable front-end that allows for easy integration with Hog. It has been tested only in version 14.7 and has some limitations, but does allow you to create, compile and keep track of the versioning of a project both locally and in the gitlab CI/CD.

### What is Hog
Hog is a set of Tcl/Shell scripts plus a suitable methodology to handle HDL designs in a Gitlab repository.

Hog is included as a submodule in the HDL repository in a `Hog` directory and allows developers to create the Vivado/PlanAhead/Quartus project(s) locally and synthesise/implement it or start working on it.

The main features of Hog are:

- a simple and effective way to maintain HDL code on git
- ensure the code was not modified before building binary files
- ensure traceability of binary files (even if produced locally)
- multi-platform compatibility, working both with Windows and Linux
- reducing code duplication creating multiple projects sharing the same top level file
- compatibility and support for IPBus
- automatic creation of Sigasi project

Hog-CI features are:

- yml files to run continuous integration in your Gitlab repository
- automatic tag creation for versioning
- automatic Gitlab release creation (including timing and utilisation reports, changelog, and binary files)
- automatic changelog in the release note
- the possibility to store the output binary files on EOS

Everything is as transparent as we could think of. Hog is designed to use just a small fraction of your time to set up your local machine and get you to work to the HDL design as soon as possible.

### Rationale
For synthesis and Place and Route (P&R) **reproducibility**, we need absolute control of:

- HDL source files
- Constraint files
- IDE settings (such as synthesis and implementation strategies)

For **traceability**, every time we produce a binary firmware file, we must:

- Know exactly how the binary files were produced
- Be able to go back to that point in the repository

To do this, Hog **automatically** embeds the git **commit SHA** into the binary file together with a more understandable **numeric version** __M.m.p__. Moreover, it automatically renames the file, including the version and inserts the hexadecimal value of the SHA so that it can be retrieved (using a text editor) in case files are renamed.

Another important principle in Hog is to **reduce to the minimum** the time needed for an external developer to **get up to speed** to work on an HDL project.
For this reason, Hog **does not rely on any external tool** or library. Only git (from version 2.9.3) and on those tools you must already have to synthesise, implement (Vivado/PlanAhead/Quartus) and simulate (Modelsim/Questasim/Riviera) the design.

To start working on any project contained in a Gitlab repository handled with Hog, you just need to:

```console
git clone --recursive <HDL repository>
cd <HDL repository>
./Hog/CreateProject.sh <project_name>
```
The project will appear in `./Projects/<project>`  and you can open it with your Vivado (ISE/Quartus) GUI.

:::{tip}
If you don't know the project name, just run `./Hog/CreateProject.sh` and a list will be displayed.
:::

### What is in the Hog directory
The Hog directory contains several Tcl and Shell scripts.

E.g. you can run:
```console
./Hog/Init.sh
```
to initialise the repository locally, following the instructions.

You can always have a look by yourself. Most of the scripts have a *-h* option to give you detailed instructions.

One of the most important script is
```console
./Hog/CreateProject.sh <my_project>
```
that creates the Vivado/PlanAhead/Quartus project locally into the `Projects` directory. When creating the project, Hog integrates a set of Tcl scripts (contained in `Hog/Tcl/integrated`) into the IDE software. If you don't know the project name, just launch `./Hog/CreateProject.sh` to find out the names of the projects in the repository.

Another useful scripts is
```console
./Hog/LaunchWorkflow.sh <my_project>
```
which runs the complete workflow of the desired HDL project, creating the binary files in the `bin` directory.

At last
```console
./Hog/LaunchSimulation.sh <my_project>
```
runs the simulation workflow. Other scripts are stored inside the `Hog/Tcl` and `Hog/Other` directories.

### Top directory
A directory called `Top` must be in the root of the repository and it contains a sub-directory for each Vivado/PlanAhead/Quartus project in the repository. These "project directories" can be arranged in a more complex structure of directories if needed.

Each of the project  directories has a fixed easy-to-understand structure and contains everything that is needed to re-create the Vivado/PlanAhead/Quartus project locally, apart from the source files that can be stored anywhere in the repository.

:::{tip}
Source files are the HDL files (.vhd, .v) but also the constraint files (.xdc, .sdc, .qsf, .tcl, ...) and the IP files (.xci, .ip, .qip, ...)
:::

### What is new in Hog 2022.2?
These are the main feature of the newest Hog2022.2 release:

- A new CI job is added to the `collect` stage, checking the timing closure of the built projects, and in case of a failed timing, it fails (with allow to failed option), signalling the users the problematic project.
- Simulation properties are now set using the new `sim.conf` file. Please refer to the [Simulation Chapter](../02-User-Manual/01-Hog-local/02b-Simulation.md) for the detailed instructions.
- A Hog version checker is launched every time the official shell scripts are sourced (`Init.sh`, `CreateProject.sh`, `LaunchWorkflow.sh` and `LaunchSimulation.sh`), checking the running version of Hog, and if a newer stable version is available, suggests the users to update.
- A hierarchical resource estimate is included in the artifacts and in the `bin` folder.
- IDE versions must be now specified in the `hog.conf` files.
- Adding option to create GitLab badges for selected projects, showing the resource utilisation, project version and timing closure status. 
- Restyled the dynamic-CI, with the option to add user jobs as in the standard CI. For more details, please have a look at the [Dynamic-CI chapter](../02-User-Manual/02-Hog-CI/06-Dynamic-CI.md)

### Contacts
Are you a git and a Tcl expert? Please join us and read the [Developing for Hog](../../04-Developing-for-Hog/01-contributing) section.

To report a [problem](../../03-Report-Suggest/01-Report) or suggest a [feature](../../03-Report-Suggest/02-Suggest), use the git issues in the [Hog git repository](https://gitlab.cern.ch/hog/Hog).
Please check in existing and solved issues before opening a new one.

For questions related to Hog, please get in touch with [Hog support](mailto:hog-group@cern.ch).

For anything related to this site, please get in touch with [Nicolò Biesuz](mailto:nbiesuz@cern.ch) or [Davide Cieri](mailto:davide.cieri@cern.ch)

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: Getting Started

01-Getting-Started/*
```

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: User Manual

02-User-Manual/*
02-User-Manual/01-Hog-local/00*
02-User-Manual/02-Hog-CI/01*
```

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: Hog Tutorial at CERN

Indico Agenda <https://indico.cern.ch/event/1038049/>
YouTube Video <https://www.youtube.com/watch?v=dDcPoeEGVdQ>
07*/*
```

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: Bug Report and Suggestions

03*/*
```

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: Developing for Hog

04*/*
Hog Doxygen Develop <https://cern.ch/hog/doxy-develop>
Hog Doxygen Master  <https://cern.ch/hog/doxy-master>
```

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: Releases

06*/*
Hog Releases <https://gitlab.cern.ch/hog/Hog/-/releases>
```

```{toctree}
:maxdepth: 1
:hidden:
:glob:
:caption: License and Authors

05-License/*
```
