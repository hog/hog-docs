# Hog general overview

In this section, we will describe how a project is set up using Hog locally and for the Continuous Integration (CI).
We will make no assumptions on the code you already have.
If you already have a repository with code in it, you can also refer to this section: [How to convert existing project to hog](../../01-Getting-Started/02-howto-convert-project/).

## Project directory structure
Hog relies on some fixed directories and file names in the HDL repository. A typical directory structure is shown in the following picture.

![](../01-Getting-Started/figures/directory_structure_new.jpg)

HDL source files, together with constraint files and simulation files can be located anywhere in the repository. In the example shown in the figure above, they are located in the `myproj` directory and, possibly, in the `library_1` and `library_2` directories.

:::{note}
The HDL source files can be stored anywhere in the repository. It is not mandatory to follow the presented folder structure. A Hog-based repository requires only the Hog submodule and a `Top` folder, organised as explained in the following.
:::

A Hog-handled repository can contain multiple (Vivado/Quartus/PlanAhead) projects, each of them corresponding to a subdirectory of the `Top` folder. Each one of these subfolders is referred to as the "top project-directory". In the figure above, we have one project called `myproj` and a group called `group` containing two projects called `proj.1` and `proj.2`.
Each top project-directory **must** contain all of the following:

- A [configuration file](01-Hog-local/01-conf) named `hog.conf`.
- A [__list__ subdirectory](01-Hog-local/02-List-files) containing the so-called list files, i.e. special text files containing the names of the source files to be included in the project.

:::{note}
From Hog2021.2, it is possible to group HDL projects inside of subfolders of the `Top` folder. For example, a subfolder `group` inside `Top` could contain two project directories (`proj.1` and `proj.2`) as shown in the figure.
:::

When you create the project, running the [`Hog/CreateProject.sh`](01-Hog-local/04-Usage), Hog reads the configuration file and the list files using Vivado, Quartus or PlanAhead and creates the complete project in another directory, in our case `Projects/myproj`. In this directory, you will find the typical Vivado/Quartus/PlanAhead file structure.

## Multiple projects or multiple repositories?
As it is now clear, a Hog-handled repository can contain multiple
projects. You may wonder when it is the case of having many projects
in the same repository and when you want to create different
repositories.

### Using different projects in the same git repository
You should use many projects in the same repository when they share a
significant amount of code.
In this case, the version of the repository will be shared among all the
projects. This is meaningful if the projects are strongly
interconnected and it is unlikely for a project to change version
without any modification to the others.

A typical use case is when the projects are intended for different devices (FPGAs) mounted on the same board.

To keep different parts of the project conceptually separated, it is possible to use many **libraries** as explained in the following.
In this case, Hog evaluates the version (and the SHA) independently for each library, so it is possible to tell at a glance if two binary files share the same library.

For example, you can have an FPGA with an "infrastructure" library
containing all the circuitry to handle communication with the external
world, and an "algorithm" library, containing the actual part of the
design that processes data. Hog libraries will allow you to tell if two
different binary files are generated using the same source code
for the algorithm but they have different infrastructure.

### Using different git repositories
If you don't have any code sharing between two HDL projects, or if the
shared code is minimal, you may think of having different
repositories.

In this case, everything will be decoupled, as the two repositories are
two completely unlinked things. All that is explained in this guide
will have to be done with both repositories and you can also, in
principle, use two different versions of Hog.

In case you have a shared part of the code, to avoid code
repetition, you can include the shared code as a git submodule.
This must be a third git repository, also independent from the
previous two.

If the code contained in the submodule is not meant to be working
stand-alone, it is not necessary to include Hog in it.

## Hog directory
The Hog repository should be included as a submodule into your HDL
repository, following these rules:

1. Hog must be in the root path of your repository
2. The directory name must be "Hog"

Moreover, it is recommended not to include the submodule protocol
explicitly, it is much better to inherit it from the repository Hog is
included into.

To obtain this you can run the following commands in the root folder of your repository.

```bash
git submodule add <protocol>://gitlab.cern.ch/Hog/Hog.git
git config --file=.gitmodules submodule.Hog.url ../Hog/Hog.git
git submodule sync
git submodule update --init --recursive --remote
```

Remember to choose your protocol among ssh, https, git, or krb5.
Also note that `../Hog/Hog.git` must be replaced with the correct path, relative to your repository.
A git error will be generated by `git submodule update --init --recursive --remote` if the path is not properly set.
Alternatively, you could add the submodule normally and then edit the `.gitmodules` file, as explained [here](../01-Getting-Started/02-howto-convert-project).

:::{note}
A mirroring of the Hog repository is available for GitLab.com users at [gitlab.com/hog-cern/hog](https://gitlab.com/hog-cern/hog)
:::

## Top-level entity
You can specify the top module with the property `top=top_module_name` inside your `.src` list file, as explained in [this section](01-Hog-local/02-List-files). If you don't specify it, Hog will use the default value `top_<project_name>`, as top module for your project. This module can be contained in any file stored anywhere in the repository as long as it is included in a `.src` list file.

Hog extracts repository information (git commit 7-digit SHA and numeric version stored in git tags) and feeds the resulting values to the design using VHDL generics or Verilog parameters.
A full list of these can be found in the [Hog generics](01-Hog-local/03-parameters-generics) section.
A template for the top-level file (in VHDL and Verilog) is available in the [Hog/Template](https://gitlab.cern.ch/hog/Hog/-/tree/master/Templates) directory.
A full description of the templates can be found in the [available templates](01-Hog-local/05-templates) section.

## Git submodules
Hog is designed to handle git submodules with no additional effort, i.e. if you use some code contained in a git repository you can simply add it to your project using the standard git command:

```bash
    git submodule add <submodule_url>
```

Submodules can be placed anywhere in the repository and files can be added to your projects through Hog [list files](01-Hog-local/02-List-files). normally[^subs].

[^subs]: Before Hog version 2021.1, there used to be restrictions on where the submodule could be located in the repository and special list files were used to include files from submodules.

## Top directory
The `Top` directory must be located in the root folder of the repository:

```bash
    Repo/Top
```

It contains one directory for each of your projects (say `proj_1`, `proj_2`, `proj_3`):

```bash
    Repo/Top/proj_1
    Repo/Top/proj_2
    Repo/Top/proj_3
```
As previously mentioned, these three directories are called the "top-project directories".

### Top-project directory
Each of the top-project directories must contain the `hog.conf` file, with the properties of the HDL project.

A template for the `hog.conf` file is available in the [Hog/Template](https://gitlab.cern.ch/hog/Hog/-/tree/master/Templates) directory.
A full description of the template can be found in the [available templates](01-Hog-local/05-templates) section.
More information on the configuration file can be found in the [`hog.conf` file](01-Hog-local/01-conf) section.

If you want some custom operation to be performed before or after the project creation (e.g. you want to create a source file using a Tcl script), you can create custom `pre-creation.tcl` and `post-creation.tcl` scripts, that should be stored inside the top-project directory. For more information, have a look at the [User Tcl section](01-Hog-local/04b-User-tcl).

### List directory
A directory named `list` must be in each of the top project-folders.
This directory contains the list files, that are plain text files, used to instruct Hog on how to build your project.
Each list file contains the names of the files to be added to the `proj_1` project.
Hog uses different kinds of list files, identified by their extension:

- `.src` : used to include HDL files belonging to the same library
- `.sim` : used to include files use for simulation of the same library
- `.con` : used to include constraint files
- `.prop`: used to set some Vivado properties, such as the number of threads used to build the project.
- `.ext` : used to include HDL files belonging to an external library

:::{note}
In `.src`, `.sim`, and `.con` list files, you must use paths relative to the repository location to the files to be included in the project.
:::

:::{admonition} Absolute or relative paths in `.ext` files?
:class: warning

By default `.ext` file use absolute paths. The only other option is to define the environmental variable `HOG_EXTERNAL_PATH`. If this is defined, Hog will interpret the entries in the `.ext` file as relative to `HOG_EXTERNAL_PATH`. To use the firmware Continuous Integration, external files must be accessible to the machine performing the git CI, e.g. can be on a protected `afs` folder.
:::

More information on the list file can be found in the dedicated [list files](01-Hog-local/02-List-files) section.

### IP and BD files
The only file needed by Hog to handle an IP is the `.xci` file for Vivado and the `.ip` for Quartus. It can be saved anywhere in the repository, but we suggest to store all the IPs in the same folder (e.g. `repo/IP`), since, for each folder containing IPs, you need to create also a `.gitignore` file ensuring that only the `.xci` file is committed to the Gitlab repository.

To add an IP core, that must be created in out-of-context mode.  The IP file shall be then saved in a folder with the same name (e.g. `repo/IP/ip_name/ip_name.xci`). Finally, you have to add the IP file to a `.src` list file of your project.

The same policy applies also to for the block-design files. For more details on the use of IPs and BDs with Hog please refer to the [dedicated section](01-Hog-local/11-IPs.md).

## Auto-generated directories
The following directories are generated at different stages of library compilation or synthesis/implementation time.
These directories should never be committed to the repository, for this reason, they are listed in the `.gitignore` file.
You can always delete any of these directories with no big consequences: they can always be regenerated by Vivado/Quartus or Hog scripts.

### The Projects folder
When you generate a project with Hog, it will create a sub-directory `Projects` here. When everything is generated, this directory contains one subdirectory for each project in the repository, containing the Vivado (Quartus) project-file `.xpr` (`.qpf`). The names of the sub-directory and the project file are always matching. In our case:

```bash
    Repo/Projects/proj_1/proj_1.xpr
    Repo/Projects/proj_2/proj_2.xpr
    Repo/Projects/proj_3/proj_3.xpr
```

The `Repo/Projects/proj_3/` directory also contains Vivado automatically generated files, among which the Runs directory:

```bash
    Repo/Projects/proj_1/proj_1.runs/
```

That contains one sub-folder for every Vivado run with all the IPs in your project, the default Vivado synthesis run (synth_1) and implementation run (impl_1).
Hog will also copy IPbus XMLs and generated binary files into `Repo/Projects/proj_1/proj_1.runs/` at synthesis/implementation time.

## Optional directories

### Doxygen
The `doxygen` directory contains the files used to generate the HDL documentation.
A file named _doxygen.conf_ should be in this directory, together with all the files needed to generate your Doxygen documentation.
VHDL is well supported with Doxygen version 1.8.13 ore later, so Hog will not use any older version.
