## Accessing ATLAS TDAQ Virtual Machines
ATLAS TDAQ provides a pre-configured Virtual Machine to be shared among all Phase-II firmware projects.
The machine is optimised for firmware building and its usage is restricted to running CI.
Individual access is hence highly discouraged and restricted.

In order to access this machine please create a [CERN service account](https://account.cern.ch/account/Help/?kbid=011010) for your project.

<img style="float: middle;" width="700" src="figures/service.png">

After the service account has been created, send an email to [atlas-tdaq-firmware-support](mailto:atlas-tdaq-firmware-support@cern.ch), so that the account is added to the VM (no sudo rights), with its home.
Once your account has been activated, you can log into the ``` atlas-tdaq-firmware-dev ``` machine.

To set up this machine to run CI/CD for your project go on the gitlab page for your project.
Go to `Settings` -> `CI/CD` and expand the `Runners` tab.
Copy the registration token in `Specific Runners` column.

On the VM, run (edit the registration token with the one you copied):

```bash
  sudo gitlab-runner register \
        --non-interactive \
        --url "https://gitlab.cern.ch" \
        --registration-token <registration token> \
        --executor "shell" \
        --description "HOG vivado runner on $HOSTNAME" \
        --tag-list "hog,vivado" \
        --output-limit 20000 \
        --run-untagged="true" \
        --locked="false"
  sudo gitlab-runner start

```
