## Setting up a Virtual Machines for firmware implementation
To run the Hog CI, you need a dedicated Virtual Machine, since the available shared runners are not ideal to execute heavy software like Vivado or Quartus.

### Setting up a dedicated Virtual Machine

In this section, you can find more information on how to set up your private Gitlab runner.
Instructions are provided assuming you have access to the CERN computing resources.
If this is not the case, you can still use Hog assuming that you have access to a machine running CentOS 7 (or another Linux flavour supported by your preferred HDL IDE) set up as a Gitlab runner.

In this case, you can ignore the next section and jump directly to [Install Gitlab runner](07-Virtual-Machines.md#install-gitlab-runner)

#### Create a CERN Openstack Virtual Machine

Openstack is a cloud operating system that controls large pools of compute, storage, and networking resources through a data-centre, managed and provisioned through APIs with common authentication mechanisms.
Openstack provides you with a [dashboard](https://openstack.cern.ch/project/) from which you can manage VM instances.

More information on Openstack can be found in the [Openstack Dashboard Documentation](https://docs.openstack.org/horizon/train/user/).

To create a new VM, you have to connect to the [CERN Openstack dashboard](https://openstack.cern.ch/project/) and create a new instance.

Openstack instances come with different flavours, meaning that you can allocate only a fixed amount of each resource to each VM.
<!-- The flavours available are usually not enough for the requirements of modern firmware implementation.
Please check the requirement for the tools and devices in your project and ask for a custom flavour.
 -->
Before creating an new instance you can add a new disk that you can use to install the needed tools.
To do this go under ``` Volumes -> Volumes ``` on the left navigation bar.
Once the Volumes summary appears you can click on ``` + Create Volume ``` and follow the instructions therein.
We recommend having at least a 40GB HD

Once you have obtained a custom flavour and a dedicated disk, you can create a new instance.
Navigate to ``` Compute > Instances ```, once you get to the instances summary click on ``` Launch Instance ```.
Fill in the required information in the form that will appear.
Under the ``` Source ``` tab  select an updated *CC7 image*: this will generate a VM running under CentOS 7.
Select the custom flavour in the ``` Flavor ``` tab.
Generate a new key pair and save the private key, this will be needed later to access your VM.

Once a new instance is running (note it might take a few minutes to be generated) attach the Volume you created to the VM.
This can be done through the drop down menu on the right side of the instance summary by clicking on ``` attach volume ```.

You can now connect to your machine through ssh.

```{note}
Your machine is not fully public yet (reference the Openstack manual for this).
This means your VM will be accessible only from the CERN domain.
If you are not on the CERN domain, connect to a CERN public machine (``` lxplus ```) and then to your machine.
```

```bash
  ssh -i private-key.pem <machine_ip_or_name>
```

Once you are logged into your machine, change the root password:

```bash
  sudo password root
```
Please follow the IT recommendation when choosing a new password.

#### Configure the Openstack VM 

Login to `lxplus.cern.ch`

```bash 
ssh -XY user@lxplus.cern.ch
```

Clone the Hog VM setup repository, in any folder inside your AFS area. 

```bash 
git clone https://gitlab.cern.ch/hog/vm-setup.git
```

```{note}
EOS is not accessible from the Virtual Machine, so don't clone the repository there.
```

Now login into your VM.

```bash
  ssh -XY user@<machine_ip_or_name>
```

Go to the just cloned `vm-setup` folder, and switch to `root` permission.

```bash
sudo su
```

Now install the required dependencies,

```bash
./install_dependecies.sh
```

Then format and mount the volume,

```bash
./volume-setup.sh
```

Finally, install the required software (Vivado, QuestaSim, Quartus) in the attached volume (`/mnt/vd`).

Follow the instructions to install them from the correspondent vendor website.

```{warning}
You are the one responsible for correctly licensing the software.
```

Once this is done, we can install our gitlab-runner. First, we need to find the gitlab-runner registering token. 

Open your Gitlab repository on a web browser, and go to `Settings->CI/CD->Runners`. Copy the registration token. 

![](../../07-Hog-Tutorial/figures/gitlab-token.png)

Go back to your virtual machine, and launch the `install_runner.sh` script, without options to see the Usage description.

```bash
 Hog - Install gitlab runner
 ---------------------------
 Install the Gitlab Runner on a linux machine

 Usage: ./install_runner.sh -u <user_name> -g <user_group> -t <gitlab_token> [OPTIONS]
 Options: --tags: <list of tags for this VM>, default: hog,vivado
          --url: <Gitlab url>. Default: https://gitlab.cern.ch

```

Required arguments for the script are:
- the user that will run the Hog-CI. If you work at CERN, we suggest to use a service account. More info [here](https://account.cern.ch/account/Management/NewAccount.aspx));
- The user group. At CERN, this corresponds to your experiment group (e.g. for ATLAS is `zp`);
- The Gitlab registering token just obtained. 

As you can see, it is possible to specify the tags for the runner and the gitlab_url, to create a runner for other Gitlab's locations (e.g. gitlab.com).

```{note}
By default, Hog requires the `vivado` and `hog` tags to run its CI. However, it is possible to change the required tags, individually for each CI job. This allows the users to direct the jobs to the desired VMs.
```

Launch now the script with the right options. This will create the home folder for your service account on the VM, and register the gitlab runner. The gitlab runner configuration will be saved on `/etc/gitlab-runner/config.toml`, and will look like this,

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "Hog runner on tutorial-hog"
  output_limit = 20000
  url = "https://gitlab.cern.ch"
  token = "<my_token>"
  executor = "shell"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```
Here you can change the concurrent value, to set the maximum numbers of gitlab CI jobs this machine will run in parallel.

If you go now back to your gitlab repository webpage, under `Settings->CI/CD->Runners`, you should see your new runner under the `Other available runners` list.

The Virtual Machine is now ready to run the Hog-CI.
