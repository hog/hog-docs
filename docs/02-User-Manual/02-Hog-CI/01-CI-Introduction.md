# Hog Continuous Integration

:::{toctree}
:glob:
:hidden:

*
:::

## Hog Continuous Integration

Hog Continuous Integration (CI) makes use of the [GitLab CI/CD tool](https://docs.gitlab.com/ee/ci/). The main features of Hog CI are:

- Control that merging branches are up-to-date with targets
- Create and builds Vivado projects
- Generate FPGA binary and report files with embedded git commit SHA
- Automatically generate VHDL code documentation using _doxygen_
- If configured, store IP generated files and implementation project results in a user-defined EOS folder
- Automatically tag the GitLab repository and, if enabled, create a release with the binary files and auto-produced changelog

Three pipelines are employed, triggered by the following actions:

- **Merge Request Pipeline**: triggered by each commit into a _non-Draft_ merge request branch
- **Master Pipeline**: triggered by each commit into the master branch
- **Tag Pipeline**: triggered by the creation of a new official tag (starting with "v\*")

:::{note}
From Hog2021.2, new commits into an already opened _non-Draft_ merge request, will interrupt the running pipeline, before starting a new one, unless the pipeline is already at the *Collect* stage.
:::

### Merge Request Pipeline
The *Merge Request* pipeline simulates, synthesises and implements the chosen HDL projects. If specified, it stores the resulting outputs to an EOS repository and creates the doxygen documentation.

The stages of the Merge Request pipeline are the following:

1. *Merge*: checks that the source branch is not outdated with respect to the target branch. If it is, the pipeline fails and asks the user to update the source branch.
2. *Generation*: the workflow of all the projects is launched.
3. *Collect*: Collects all the artifacts from the previous stages and, if enabled, produces the Doxygen documentation and copies the implementation outputs and the Doxygen documentation to the EOS repository (optionally). Finally a Job is run to check that all the projects meet the timing, if that is not the case the job fails (but not the pipeline).

During the *Generation* step, Hog-CI performs the following steps:

- Creates a Vivado/Quartus project for each project specified in the `.gitlab-ci.yml` file. It checks also that the Hog submodule in the repository is the same as the one specified in the CI configuration file and that the required Hog environmental variables are set up. Finally, for Vivado projects, it checks the syntax of the HDL codes, before moving to the next stage.
- Optionally handles the IPs, if the __HOG_IP_PATH__ environment variable is set, as explained in [Environment variable section](02-setup-CI.md#environment-variables)
- Synthesises the projects. An option to run only up to the synthesis stage is available.
- Implements the projects and creates the bitstreams. An option to disable the bitstream creation is available. During this stage the implementation timing results and project version are also written in the merge request page.

Hog also supports a dynamically-generated CI. For more details, refer to the [Dynamic CI Section](06-Dynamic-CI.md).

From Hog2021.1, users can also add custom jobs to the MR pipeline, before and after the *Generation* stage, and after the *Collect* stage. For more details, see the [Custom User CI Stages section](04b-User-Stages.md).

### Master Pipeline
The *Master* pipeline consists only of one stage (*Merge*), which tags the repository according to the Merge Request description. Assuming the latest tag was *vA.B.C*, the pipeline will

* increase A, if the MR description contains the keyword
"MAJOR_VERSION"
* increase B, if the MR description contains the keyword
"MINOR_VERSION"
* increase C, in the other cases.

### Tag Pipeline
The *Tag* pipeline, is triggered every time a new tag is created.

The goal of this pipeline is to create a release (if enabled) and archive the product of the firmware workflow, including the binary files.  On the Release the timing results and the project versions are also added.

If EOS is enabled, Hog-CI also copies the binary files and logs to the EOS official storage, creating a new sub-folder with the name of the new tag.

From Hog2022.2, this stage creates optionally GitLab badges in the homepage of your GitLab repository, showing the resource utilisation and the timing closure status for the project defined in the `HOG_BADGE_PROJECTS` CI/CD variable. An example, of the resulting badges is shown in the following figure. 
