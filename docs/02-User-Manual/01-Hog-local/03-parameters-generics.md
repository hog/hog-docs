## Parameters/Generics

At creation time and just before the synthesis starts[^presynthesis][^planahead], Hog feeds a set of value to the design.

This is done to link the binary file with the state of the repository at the moment of synthesis.
To do this Hog exploits VHDL generics or Verilog parameters[^generics].
In this section the details of these generic/parameters are explained.

The values of these generics/parameters should be connected to dedicated registers that can be accessed at run time on the device (e.g. IPBus registers).

To access the Hog generics/parameters you must define the following in your top level entity:

| Generics/parameters name | Generics type (VHDL only) | Generics/parameters size | Generics/parameters description                                                                                                                              |
|:-------------------------|:-------------------------:|:------------------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `GLOBAL_DATE`            | `std_logic_vector`        | 32 bit                   | Date of last commit when the project was modified. Format: ddmmyyyy (hex with decimal digits, no digit greater than 9 is used)                               |
| `GLOBAL_TIME`            | `std_logic_vector`        | 32 bit                   | Time of last commit when the project was modified. Format: 00HHMMSS  (hex with decimal digits, no digit greater than 9 is used)                              |
| `GLOBAL_VER`             | `std_logic_vector`        | 32 bit                   | Last version Tag when the project was modified.  The version of the form m.M.p is encoded in hexadecimal as MMmmpppp                                         |
| `GLOBAL_SHA`             | `std_logic_vector`        | 32 bit                   | Git hash (SHA) of the last commit when the project was modified.                                                                                             |
| `TOP_VER`                | `std_logic_vector`        | 32 bit                   | Top directory version, containing the [hog.conf](01-conf.md) file and other files.  The version of the form m.M.p is encoded in hexadecimal as MMmmpppp      |
| `TOP_SHA`                | `std_logic_vector`        | 32 bit                   | Top directory version, containing the [hog.conf](01-conf.md) file and other files.                                                                           |
| `CON_VER`                | `std_logic_vector`        | 32 bit                   | The version of the constraint files.  The version of the form m.M.p is encoded in hexadecimal as MMmmpppp                                                    |
| `CON_SHA`                | `std_logic_vector`        | 32 bit                   | The git commit hash (SHA) of the constraint files.                                                                                                           |
| `HOG_VER`                | `std_logic_vector`        | 32 bit                   | Hog submodule version.  The version of the form m.M.p is encoded in hexadecimal as MMmmpppp                                                                  |
| `HOG_SHA`                | `std_logic_vector`        | 32 bit                   | Hog submodule git commit hash (SHA).                                                                                                                         |
| `XML_VER`                | `std_logic_vector`        | 32 bit                   | (optional) IPbus xml version.  The version of the form m.M.p is encoded in hexadecimal as MMmmpppp                                                           |
| `XML_SHA`                | `std_logic_vector`        | 32 bit                   | (optional) IPbus xml git commit hash (SHA).                                                                                                                  |
| `<MYLIB>_VER`            | `std_logic_vector`        | 32 bit                   | (one per library, i.e. .src list file) Version of the files contained in the .src file.  The version of the form m.M.p is encoded in hexadecimal as MMmmpppp |
| `<MYLIB>_SHA`            | `std_logic_vector`        | 32 bit                   | (one per library, i.e. .src list file) Git commit hash of the files contained in the .src file (SHA).                                                        |
| `<MYEXTLIB>_SHA`         | `std_logic_vector`        | 32 bit                   | (one per external library) Git commit hash (SHA) of the .ext file.                                                                                           |
| `FLAVOUR`                | integer                   |                          | (integer) flavour used for generating this bit file, set if your project uses Hog flavours to produce bit files for different devices                        |

The firmware date and time are encoded to be readable in hexadecimal so 0xA, 0xB, 0xC, 0xD 0xE, and 0xF are not used. For example the date 5 July 1952 is encoded as 0x05071952 and the time 12.34.56 is encoded as 0x00123456. To guarantee synthesis reproducibility, Hog uses the last-commit date and time rather than the synthesis date and time.

Names ending with `_SHA` are used for the 7-digit SHA of the git commit, being the SHA an hexadecimal number there is no ambiguity in its conversion. Names ending with `_VER` are used for a numeric version of the form M.m.p encoded in hexadecimal as 0xMMmmpppp. So for example version 7.10.255 becomes 0x070A00FF.

The version and hash of a subset of files is calculated using `git log` and means the latest commit (and the latest version tag) where at least one of the files was changed.

It is worth noticing that there is no one-to-one correspondence between tag and hash, because not all the commits are tagged, so a tag can correspond to several hashes, all the ones that occurred between that tag and the previous one.

Hog will provide all the parameters/generics described in the table above, but if you do not plan to use them you can just leave them unconnected or do not add them to your top module at all.
The HDL synthesiser will ignore them, maybe giving a warning.[^planaheadgenerics]

[^presynthesis]: Note that under Quartus the script is run as `PRE-FLOW` script. This means the full compilation flow must be launched to assign default values to the generics. Launching single steps might cause the compilation to fail.

[^planahead]: Unlike in Vivado, planAhead/ISE does not support built-in pre-synthesis scripts. In this case, the Hog pre-synthesis, pre-implementation, pre/post-bitstream scripts are run by Hog tool itself, which means that assignment of generics and other Hog build feature *only* happen when using Hog from the command line or from the CI. Firmware builds made in the GUI will not have the Hog generics populated.

[^planaheadgenerics]: ISE/planAhead handles this somewhat problematically: assignment of a non-existent generic in planAhead creates a HDLcompiler error. To use Hog with planAhead you need to make sure that all of the Hog generics are exposed in the top level design for synthesis.

[^generics]: Generics are used in VHDL language, parameters are used in Verilog, SystemVerilog languages.
