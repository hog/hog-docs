## Custom User Tcl scripts in build flow

Hog runs automatically some scripts every time the compilation workflow is started or during any step in the workflow.
It is also possible to run user scripts during any compilation step; they will be run immediately after the Hog scripts.

The user scripts shall be named as:

- `Top/<project name>/pre-synthesis.tcl`: it will be run before project synthesis, after Hog pre-synthesis script;
- `Top/<project name>/post-synthesis.tcl`: it will be run before project synthesis, after Hog post-synthesis script;	
- `Top/<project name>/pre-implementation.tcl`: it will be run before project implementation (Vivado/ISE), after Hog pre-implementation script;
- `Top/<project name>/post-implementation.tcl`: it will be run after project implementation (Vivado/ISE) or project fit (Quartus), after Hog post-implementation script;
- `Top/<project name>/pre-bitstream.tcl`: it will be run before bitstream after Hog pre-bitstream script;
- `Top/<project name>/post-bitstream.tcl`: it will be run after bitstream, after Hog post-bitstream script.
