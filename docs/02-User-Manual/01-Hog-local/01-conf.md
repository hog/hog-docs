## Project configuration file

Hog reads all the project properties from a configuration file called `./Top/<my_project>/hog.conf` to generate the HDL project.

The `hog.conf` file is expected to contain at least few basic variables with the information needed to build your project.
This section contains a full recipe to write a `hog.conf` file for your project.

Templates for a Vivado, PlanAhead and Quartus project can be found under `./Hog/Templates/hog_*.conf`.

This is an example of the minimal `hog.conf` file needed for a Vivado project, specifying only the device:
```ini
#vivado 2020.2
[main]
PART = xc7a35tcpg236-1
```

### Telling Hog the IDE tool to be used

The first line of your configuration file is expected to be a comment indicating which IDE tool and what version must be used to generate your project.
The following tools are recognised:

- vivado
- planahead
- quartus

After the name of the tool, the version must be specified as in the following examples:
```ini
#vivado 2020.2
#quartus 19.1.0
#planahead 14.7
```

The version must be in the format `<a>.<b>[.<c>]`, i.e. it can be 2 or 3 numbers separated by dots. There must be at least a space between the IDE name and the version, while the spaces between the '#' and the compiler name are optional, so the following syntaxes are also acceptable:

```ini
#vivado 2019.2.1
# vivado 2019.2
#  vivado   2019.2
```

If this line is not available Hog will assume your project is a Vivado project, but will give you a critical warning.

#### Why do wee need the tool version?
Specifing the version is important to ensure that the project is run with the required tool version.

Project properties specified in the `hog.conf` file might be incompatible among different IDE versions.
More importantly the Intellectual Properties (IPs) are linked to a specific version and are not inter-version compatible.

If you use a tool that is older than the one specified in `hog.conf` Hog will simply refuse to create the project and throw an Error. In this case, if you really want to create the project, you can modify the version in `hog.conf` and retry... The probability of success are tiny thoug.

If you use a newr tool than the one specified, Hog will create the project but will throw a critical warning, reminding you that if you mean to upgrade your project to work with a newer tool version, you also need to update the version number in `hog.conf`. If the project contains IPs you will have to update those too and possibly modify some project properties in `hog.conf`.

:::{warning} PlanAhead and Quartus are currently partially supported.
:::

### Main section: project variables

The configuration file needs to contain a `main` section specifying at least the following variables:

#### PART
The `PART` variable indicates the target device code for your project.
This variable is *mandatory*.
It must be chosen among the ones provided by the chosen HDL compiler.
As an example for a Xilinx Virtex-7 FPGA it could be set to `xc7vx330tffg1157-2`.
Note that the exact code will depend on the full characteristics of the device you are using, e.g. number of logic cells, package, speed grade, etc.

#### FAMILY
The `FAMILY` variable indicates the device family.
This variable is mandatory for *Quartus only*.
The value must be chosen among the ones provided by the chosen HDL compiler.
As an example for a Intel MAX10 FPGA it must be set to "MAX 10".
Please note that the variable value is included in quotation marks.

### Synthesis and implementation sections (Xilinx only)
In Vivado you can set properties at 3 levels: project, synthesis, implementation.
To do this you need to create the specific section of your hog.conf file: `main`, `synth_1`, `impl_1`.

This is an example of a configuration file for a Vivado project that includes several properties:

```ini
#vivado 2020.2
[main]
PART = xc7vx550tffg1927-2

[synth_1]
STEPS.SYNTH_DESIGN.ARGS.RETIMING = true

[impl_1]
STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE = 1
STEPS.OPT_DESIGN.ARGS.DIRECTIVE = Default
```

To find out the exact name and value of the property, use Vivado GUI to click on the checkbox you like.

![](./figures/tick_gui.png)

This will make Vivado run the set_property command in the Tcl console.

![](./figures/Vivado_tcl.png)

Then copy and paste the name and the values from the Vivado Tcl console into the lines below.

:::{admonition} Vivado Strategies
Vivado strategies are in reality a set of directives that changes different parameters. Unfortunately, Hog is not able to compare the properties set by the strategies against the one specified in the `hog.conf` file. For this reason, if you specify the `STRATEGY` inside the `hog.conf`, you will get a Warning, notifying that the config check is disabled.

To avoid this, we suggest to set the Strategy in Vivado and then regenerate the `hog.conf` file using the provided button. The new file will then contain all the properties of the chosen strategy, without the `STRATEGY` parameter itself.
:::

### Parameters section (Xilinx only)
Vivado projects contain volatile parameters that must be set before launching each run: synthesis and implementation.

The `parameters` optional section is used for this purpose. The most important volatile parameter is MAX_THREADS that specifies how many cores to use in synthesis and implementation.

By default Hog sets this value to 1 to assure that the binary files production is deterministic.

This is an example of the parameters section:

```ini
[parameters]
MAX_THREADS = 16
```

### Hog section
The optional `Hog` section is used to specify Hog directives valid for the project.

The supported properties are:

- `ALLOW_FAIL_ON_CONF`: will not set the version to 0 in firmware registers, even if the conf file is modified with respect to the project (The Critical Warning is still raised)
- `ALLOW_FAIL_ON_LIST`: will not set the version to 0 in firmware registers, even if any of the list files is modified with respect to the project (The Critical Warning is still raised)
- `ALLOW_FAIL_ON_GIT`: will not set the version to 0 in firmware registers, even if a file is found in the project which is not committed to the repository (The Critical Warning is still raised)
- `EXPORT_XSA`: will either force or prevent the export of an XSA file for Xilinx Zynq devices (documentation [below](#xsa-export-for-zynq-devices-xilinx-only))

## Running additional scripts
If you wish to run some scripts before creating your project you can add a Tcl file called `./Top/<my_project>/pre-creation.tcl`.
This can be used to generate additional files to be included in your project.

If you want to run a script after the project is created, you can add a Tcl file called `./Top/<my_project>/post-creation.tcl`.

This can be used, for example, to integrate a pin assignment in your Quartus project. To do this export your pin assignment as a Tcl script form the Quartus Pin planner (File > Export) and source the resulting script after calling `./Hog/Tcl/create_project.tcl`.

:::{note}
It is possible to add (or generate) files to the project using these scripts. From Hog2022.1, you can generate any kind of files using `tcl` scripts. Hog will save the md5sum of the generated files at creation time into a `.hog` folder, inside the `Projects` directory. At synthesis time, the md5sum will be compared against what it has been saved, and if different a Warning message will be printed.
:::

### XSA Export for Zynq Devices (Xilinx Only)

For Xilinx Zynq devices, integration of the firmware with the Linux system uses a Xilinx Support Archive (XSA) file.

An XSA file is a zip archive containing files such as Vivado tool versions, part numbers, the compiled firmware bitstream, C headers, and the Hardware Handoff File (HHF) that contains information describing the Processor-to-FPGA-interface implemented in the Vivado Block Designer.

Hog supports the automatic generation of XSA files for Zynq devices.

In the post-bitstream stage of compiling Zynq firmware, Hog will check the FPGA part number. If it is determined to be a Zynq device, Hog will automatically run the `write_hw_platform` command which is used to export an XSA file from a Vivado project.
Detection of a Zynq part is using a simple regex for parts beginning with `xc7z` or `xczu`; this will need to be updated in the future as new parts are released by Xilinx.

If automatic detection of a supported device fails, generation of an XSA file can be forced with the `EXPORT_XSA` property in `hog.conf`.
Conversely, if export of XSA files is not desired, it can be disabled with the same property.

``` ini
[hog]
# set true to force creation of an XSA
EXPORT_XSA=true
# set false to stop creation of an XSA
EXPORT_XSA=false
```