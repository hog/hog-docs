## Hog buttons
Hog buttons can be used to check and update Hog [list files](02-List-files) and the `hog.conf` [configuration file](01-conf).
![Hog buttons](figures/hog_custom_button.png)

:::{warning}
This feature is not implemented for Quartus or PlanAhead projects.
:::

### Adding the buttons to the GUI
To add the buttons to your Vivado GUI you can run the `hog/Init.sh` script and answer yes at the appropriate question.

Alternatively you can run the Tcl script yourself from your Vivado Tcl console:

```tcl
source ./Hog/Tcl/utils/add_hog_custom_button.tcl
```

or from a bash console:  

```tcl
vivado -mode batch -notrace -source ./Hog/Tcl/utils/add_hog_custom_button.tcl
```

### Button functionalities
Currently there are 3 Hog custom buttons:
- **CHK**: Checks that the project files and properties match what is defined in the `Top` directory.
- **LIST**: Recreates all the list files of your projects to match the current project.
- **CONF**: Recreates the `Top/[group name]/<project name>/hog.conf` and the `Top/[group name]/<project name>/sim.conf` files to match the current project.

#### The check button
The **CHK** button can be used to double check if the current project matches with the content of the list and conf files, i.e. what it is going to be committed to the repository.

If you have been adding new files, trying different strategies, or setting up several values in Vivado properties, you can use this button to check if anything you have done differs from the list and conf files.

#### The list button
If you have added new files to the project using Vivado GUI, you can press the **LIST** button to update Hog's list files. Remember to commit them to git after you have done that.

:::{warning}
If you are using recursive list files, i.e. list files which include other list files, Hog will not be able to recreate your hierarchical structure and will flatten it.
:::

#### The configuration button
If you have set any non-default property in your project, you can use the **CONF** button to update your `hog.conf` and `sim.conf` files. Please always double check that the values in the new files are the ones you expect, as this feature is relatively new. If something is not as you expect it, please report it to the Hog team.
