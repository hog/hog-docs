## Simulation 
Hog supports all simulation software currently supported by Vivado (click [here](https://www.xilinx.com/products/design-tools/vivado/simulation.html) for the full list). Hog simulation has been tested with Vivado Simulator (also called Xsim), Mentor Graphics Questasim, Mentor Graphics Modelsim and Aldec Riviera Pro. 

The simulation setup is always handled by Vivado, as if the simulation button on the GUI was clicked, then the simulation run is done with the simulation software.

```{note}
Currently the simulation is not supported under Quartus. A complete support will be included in the next releases.
```

### Simulation sets
Simulations are organised in so-called "simulation sets" as in Vivado.

![simulation sets](figures/simulation_sets.png)

In this figure, you can see an example of simulation sets, these are contained in Vivado in the `Simulation_Sources` folder in the hierarchy view.
You can right click on each of them, set it as active (becomes bold) and run it by clicking the "Run simulation button". In the figure above `algo_sim` is the active simulation.

Every simulation set has a top entity, the test bench containing the device under test, and can be simulated independently.

In Hog every simulation set corresponds to a `.sim` [list file](02-List-files.md).


<!-- In the `.sim` list-files, in addition to the properties defined for the `.src` list files, some other properties are specified.
These properties are:

- `topsim=<entity_name>`: (mandatory) the name of the entity that will be the top level of the simulation;
- `runtime=<time>`: (optional) it indicates the running time for your simulation;
- `wavefile`: (optional) it indicates the name of the entity you want to set as top level in your simulation (Questasim/Modelsim only);
- `dofile`: (optional) it indicates the file containing the signal waveforms to be observed in your simulation (Questasim/Modelsim only).

From Hog2022.2, all simulation settings can be set inside the `sim.conf`. Please refer to [hog.conf section](01-conf.md#simulation-configuration-file-xilinx-only) for more information. It is recommended to set all properties using the `sim.conf` file, since Hog has full control on them, and the properties in the `.sim` list file will be deprecated in future releases.
 -->

### Simulation Configuration File
The simulation properties are read from another file called `./Top/<my_project>/sim.conf`, with a similar syntax as the `hog.conf`. The need for a separate file lies on the fact that touching the simulation properties does not affect the actual firmware which will be synthesised. Thus we don't want Hog to change the version of the project, when the developers touch only the simulation. 

Simulation properties can be set individually for each simulation set, or globally for all of them. For example, to set `xsim.elaborate.rangecheck` property for the simulation set `sim_1`, you should write

```ini
[sim_1]
xsim.elaborate.rangecheck = true

```

Alternatively, if you want to set this property for all your simulation sets, you should write.

```ini
[sim]
xsim.elaborate.rangecheck = true
```

A typical `sim.conf` file would look like then:

```ini
[sim]
XSIM.ELABORATE.RANGECHECK = False

[my_sim]
ACTIVE=1
MODELSIM.COMPILE.VHDL_SYNTAX=2008
MODELSIM.CUSTOM_UDO=file1.do
MODELSIM.SIMULATE.CUSTOM_UDO=file1.do
MODELSIM.SIMULATE.CUSTOM_WAVE_DO=file1.tcl
MODELSIM.SIMULATE.RUNTIME=500us
TOP=tb_my_sim
```

Please note that the for the settings that require the name of a file, for instance `CUSTOM_UDO` or `CUSTOM_WAVE_DO`, only the file name must be set, and not the full path. Remember also to include these files in your `.sim` list file, in order to add them to the Vivado project. 

Hog also offers the possibility to generate the `sim.conf` automatically, by clicking on the [Hog Conf Button](02a-Hog-Buttons.md) in the Vivado GUI.

Remember that individual simset properties have always the priority with respect to the global. So you can set a global property for all your simsets, and change it just for one of them, for example.

To set a particular simset as the active one, you need to set the `ACTIVE` property to `1` in your `sim.conf`, under the relative simset section. If no active simset is specified, Hog will set as active the last created simulation set.

```{note}
In Hog versions older than `Hog2022.2`, only a limited set of simulation properties could be specified. These properties were directly set inside the `.sim` files and were:

- `topsim=<entity_name>`: (mandatory) the name of the entity that will be the top level of the simulation;
- `runtime=<time>`: (optional) it indicates the running time for your simulation;
- `wavefile`: (optional) it indicates the name of the entity you want to set as top level in your simulation;
- `dofile`: (optional) it indicates the file containing the signal waveforms to be observed in your simulation.

In Hog2022.2, properties defined in the `sim.conf` files have always the priorities, but the tool will still set those that are defined in the `.sim` list files. Altough, please note, that we plan to deprecate this feature in future releases, where only the `sim.conf` file will be supported. 
```

### Project simulator
The software used to simulate is set at project level in Vivado. To change it you have to change the "target_simulator" property in the "main" section of your [hog.conf file](01-conf.md).

```{note}
Vivado does not allow to run different simulations with different simulators automatically. If you want to do that locally, you have to go every time to the simulation settings to switch from one simulator to another. The setting will be valid at project level, so for all the simulations sets. If you change the simulator by clicking in Vivado GUI, the change will not be propagated to the repository.
```

#### Simulation Library
When using an external simulation software, it is necessary to specify the path to the compiled simulation library. By default, Hog sets this path to `repo_path/SimulationLib`, where `repo_path` is the root folder of your git repository. In the case that the `HOG_SIMULATION_LIB_PATH` environmental variable is defined, as in the Continuous Integration (see [here](../02-Hog-CI/02-setup-CI.md)), Hog will set the path to it.

Finally, it is also possible to specify the simulation library path, when creating the project, using the `-l` or `--lib` flag

```bash
    ./Hog/CreateProject.sh <myproj> -l <library_path>
```

### Simulation in the CI
If the simulation job is configured in your projects CI, the
[Hog CI](../02-Hog-CI/01-CI-Introduction.md) will automatically run the simulation sets.

The CI stage will fail if one simulation fails. This means that, in order for the CI stage to be meaningful, each simulation set should be designed to fail if some unwanted behaviour is present.

Even if Vivado does not allow for different simulators in the same project, in Hog CI it is possible to specify a different software to be used in each set. This can be done by adding at the top of the `.sim` list one of the following lines:

```
    #Simulator xsim       # For Vivado Simulator
    #Simulator questa     # For QuestaSim Simulator
    #Simulator modelsim   # For ModelSim Simulator
    #Simulator riviera    # For Aldec Riviera Simulator
    #Simulator activehdl  # For Aldec Active-HDL Simulator
    #Simulator ies        # For IES Simulator 
    #Simulator vcs        # For Synopsys VCS Simulator

    #Simulator skip_simulation #To skip the simulation in the CI
```
If you do not specify the simulator software, Hog will set ModelSim as default.

If for any reason, you do not want a specific simulation set to be simulated by the CI, just use `skip_simulation`.

An example `.sim` list file looks like this
```
    #Simulator Xsim
    tb_source_dir/tb_for_lib1.vhd 
    wave_source_dir/wave_lib1.tcl 
    do_source_dir/dofile_lib1.do
    tb_source_dir/another_file.vhd
```

### Simulating without GUI
If you want to run the simulation locally, but without using the GUI (i.e. in the same way that CI does), you can use the `Hog/LaunchSimulation.sh` wrapper script.
```bash
    ./Hog/LaunchSimulation.sh <project name> [-lib_path <sim lib path>] [-simset <sim sets>] [-quiet] [-recreate]
```
This script will launch all the simulation sets in the specified projects, using the software specified in the `.sim` file and skipping the simulation if `skip_simulation` is specified.
More details can be found [here](04-Usage.md#simulating-with-shell).
