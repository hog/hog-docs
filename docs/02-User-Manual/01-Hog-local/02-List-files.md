## List Files
A directory named `list` must be in each of the top project-folders.
This directory contains the list files, that are plain text files, used to instruct Hog on how to build your project.
Each list file shall contain the list of the files to be added to the project.

Some properties can also be specified in the list files by adding them after the file name, separated by any number of spaces.
Comments can be added using `#`.
A generic list file looks therefore like this,

```
    source_dir/file1.vhd [prop_1] [prop_2]
    # comment here, for example
    source_dir/file2.vhd [prop_3]
    source_dir/file3.vhd
```

Hog also supports the use of simple wildcards to specify source files.  For example, the three source files above could be added with:

```
    source_dir/*.vhd [prop]
```

Note that a property applied to a wildcard source file will be applied to all source files found.
Wildcards can be used in the path as well as the filename, e.g. ```*/src/*.vhd```.
During project creation, the build scripts will print out a note with the files found by the wildcard expansion. Currently there is no way to exclude files from the wildcard (a la in a `.gitignore` file) so you should make sure you are not adding unwanted files.

Hog uses different kinds of list files, identified by their extension:

- `.src` : used to include HDL files belonging to the same library
- `.sim` : used to include files use for simulation of the same library
- `.con` : used to include constraint files
- `.ext` : used to include HDL files belonging to an external library

:::{note}
In `.src`, `.sim`, and `.con` list files, you must use paths relative to the repository root directory.
:::

### Source list files (.src)
Hog creates a HDL library[^ise_library] for each `.src` list file and assign all the file included in the list file to that library.

For example, if there is a `lib_1.src` file in the list directory like this:

```bash
    source_dir/file1.vhd
    source_dir/file2.vhd
    source_dir/file3.vhd
    IP/ip1.xci
    IP/ip2.xci
```

the files will be included into the Vivado project in the `lib_1` library.

To use them in VHDL[^library] you should use the following syntax:

```vhdl
library lib_1;
use lib_1.all;

...

  u_1 : entity lib_1.a_component_in_lib1
  port map(
    clk  => clk,
    din  => din,
    dout => dout
    ):
```

For Vivado projects the following properties can be specified for files in a `.src` list:

- `top=<top module name>`: Defines the name of the top module of the project.
- `93`: (only for `.vhd` and `.vhdl` files) File type is VHDL 93. If not specified Hog will use VHDL2008[^planahead].
- `XDC`: File Type is XDC.
- `nosynth`: File will not be used in synthesis.
- `noimpl`: File will not be used in implementation.
- `nosim`: File will not be used in simulation.
- `locked`: **ONLY FOR `.xci` FILES**, the IP will not be re-synthesised by the CI. More details in the [IP section](11-IPs.md).
- `verilog_header` (**From Hog2022.1**): File type will be set to Verilog header.
- `SystemVerilog` (**From Hog2022.1**): File type will be set to System Verilog.
- `source` (**From Hog2022.1**): Only for Tcl file. The Tcl script will be sourced by Vivado.

[^planahead]: Note that VHDL2008 is not supported by the planAhead/ISE synthesis tools.

For Quartus projects the following properties can be specified for files in a `.src` list:
- VHDL files:
  - `1987`: File type is VHDL 1987. If not specified Hog will use project default HDL version[^quartus_default];
  - `1993`: File type is VHDL 1993. If not specified Hog will use project default HDL version;
  - `2008`: File type is VHDL 2008. If not specified Hog will use project default HDL version;
- SystemVerilog files:
  - `2005`: File type is SYSTEMVERILOG 2005. If not specified Hog will use project default HDL version;
  - `2009`: File type is SYSTEMVERILOG 2005. If not specified Hog will use project default HDL version;
- Verilog files:
  - `1995`: File type is VERILOG 1995. If not specified Hog will use project default HDL version;
  - `2001`: File type is VERILOG 2001. If not specified Hog will use project default HDL version;
- Tcl macro files:
  - `qsys` indicates this file has to be used to generate an Intel Platform Designer system (`.qsys`)
- Intel Platform Designer system (`.qsys`)
  - `nogenerate` do not generate the system

[^ise_library]: https://www.xilinx.com/support/documentation/sw_manuals/xilinx11/ise_c_working_with_vhdl_libraries.htm
[^library]: Libraries are ignored in Verilog and SystemVerilog
[^quartus_default]: The Intel Quartus Prime default HDL version can be specified adding a specific command in your .qsf file, e.g. Add `set_global_assignment -name VHDL_INPUT_VERSION VHDL_2008` to a .tcl  listed in an appropriate .con list file. For the specific command to be used refer to the Quartus Prime scripting reference manual.

#### Intel Platform Designer systems

Intel Platform Designer system files, either `.qsys` or `.tcl` must be added to the `.src` list files.
The user has different options to add the handle Intel Platform Designer files.

If a `.qsys` file is listed, Hog tries to generate the system by running the `qsys-generate` command.
The properties of the `.qsys` files are used as command options for the `qsys-generate` command *as they are*, e.g. to use VHDL for synthesis then the list file must contain:

```
  <qsys_path>/<qsys_name>.qsys --synthesis=VHDL
```
:::{note}
Users must never use the `--output-directory` option which is automatically set by Hog.
:::

The system is generated in a folder having the same name and path of the .qsys file, i.e. the file `<qsys_path>/<qsys_name>.qsys` will be generated in the `<qsys_path>/<qsys_name>` folder.
After generating the IP variations, Hog will automatically find and add the generated `.ip`, `.qip`, `.v`, `.vhdl` files to the project.
Hog will search for IP files in the output directory and in the synth directory, i.e. `<qsys_path>/<qsys_name>` and `<qsys_path>/<qsys_name>/synth`

In this case the output folder (`<qsys_path>/<qsys_name>`) must be listed in the `.gitignore` file.
:::{note}
All the information relative to the qsys system generation will be logged into the file: `<qsys_path>/<qsys_name>.qsys-generate.log`.
:::

To add a .qsys file without generating the ip variation please specify the `nogenerate` option.
Note that in this case Hog  will not automatically add ips to the project.
The IPs must be generated and listed in the .src list file by the user.

In this case there are no requirements on the .gitignore file.

Users can also modify/create system using a dedicated .tcl command macro.
In this case the user must list this file using the `qsys` file property.
Hog will automatically run 'qsys-script' to generate the IP variation file (.qsys).
The .qsys file will be generated in the same folder of the .tcl macro, i.e. `<qsys_path>/<qsys_name>.tcl` will be used to generate `<qsys_path>/<qsys_name>.qsys`.
Hog assumes the script has the same name as the qsys system, i.e. the `<qsys_path>/<qsys_name>.tcl` will be used to generate `<qsys_name>` system.
Once the .qsys file has been generated it is added to the project and treated as if it was listed in a .src file, i.e. IPs are generated and added to the project.
To pass any options to the `qsys-generate` command list them after the `qsys` property, e.g. to specify to use VHDL for synthesis then the list file must contain:

```
  <qsys_path>/<qsys_name>.tcl qsys --synthesis=VHDL
```

:::{note}
When using .tcl scripts for Intel Platform Designer system generation both the .qsys and the generated folder must be listed in the .gitignore file.
:::

:::{note}
All the information relative to the qsys system creation will be logged into the file: `<qsys_path>/<qsys_name>.qsys-script.log`.
:::

### Simulation list files (.sim)
In this files are listed all the HDL files used only for simulations.
These include test benches and other files that are not used in
synthesis: for example modules used to read and write from and to files.

For each `.sim` file, Hog creates a so-called "simulation set".

For more information about simulation and `.sim` simulation list-files
see the [dedicated section](02b-Simulation.md).

### Constraint list files (.con)
All constraint files must be included by adding them into the `.con` files.
Both `.xdc` (for Vivado), `.sdc`, `.qsf` and `.tcl` files can be added here.
For Vivado the `nosynth` and `noimpl` properties can be specified here, if required.

:::{note}
While creating a Quartus Prime Project all `.qsf` and `.tcl` file included in a `'con` list file will be automatically sourced. This can be used to apply global project settings or include a pinout file. To add a Macro Library to be used during the design phase create a dedicated `.src` list file.
:::

For example, a `.con` file looks like:
```
  constr_source_dir/constr1.xcf           #constraint used for synthesis and implementation
  constr_source_dir/constr2.xcf nosynth   #constraint not used in synthesis
  constr_source_dir/constr3.xcf noimpl    #constraint used in synthesis only
```

### List files are recursive
A list file can contain another list file, whose content will be then included.

You can include a mixture of source files and list files, Hog will recognise them by the extension.

This feature can be used if multiple projects on the same repository share a subset of files. A common list file can be created and stored anywhere in the repository and then included in the specific project list files.

If the content of a list file is added recursively, by default Hog uses the file name of the **included** list file as library name. A custom name can be specified by using the `lib` keyword as a property of the included list file in the including list file. This is quite convoluted, hopefully the following example will help.

For example, say that `foo.src` includes `bar.src` that in turn includes `file.vhd`, by default the library of `file.vhd` (and of all other files contained in `bar.src`)  is `bar`.

To change it to a custom name, the following syntax can be used inside `foo.src`:

```
file_a.vhd
file_b.v
...

bar.src lib=my_library

...

```
Now Hog will set the `my_library` for all the VHDL files contained in `bar.src`.

:::{note}
This feature is not implemented for Quartus projects.
:::

### External list files (.ext)

External proprietary files that are protected by copyright and cannot be published on the repository shall be included using the `.ext` list file. This procedure has to be used __ONLY__ in the exceptional case of files that can not be published because of copyright.

The `.ext` list file has a special syntax since the md5 hash of each file must be added after the file name, separated by one or more spaces:

```
/afs/cern.ch/project/p/project/restricted/file_1.vhd  725cda057150d688c7970cfc53dc6db6
/afs/cern.ch/project/p/project/restricted/file_2.xci  c15f520db4bdef24f976cb459b1a5421
```

The md5 hash can be obtained by running the md5sum command on a bash shell

```bash
  md5sum <filename>
```

the same checksum can be obtained on the Vivado or QuartusPrime tcl shell by using:

```tcl
  md5::md5 -filename <file name>
```

Hog, at synthesis time, checks that all the files are there and that their md5 hash matches the one contained in the list file.

:::{admonition} Absolute or relative paths in `.ext` files?
:class: warning

`.ext` list files must use absolute paths or relative to __HOG_EXTERNAL_PATH__ folder, if defined. To use the firmware Continuous Integration, this path must be accessible to the machine performing the git CI, e.g. can be on a protected `afs` folder.
:::

:::{note}
This feature is not implemented for Quartus projects.
:::
