## Additional Tcl Scripts

Hog provides a series of Tcl scripts, which executes different common tasks. These scripts are located in `./Hog/Tcl/utils`.
Depending on the HDL IDE, the scripts can be run as:

**Vivado batch mode**
```bash
  vivado -mode batch -notrace -source <tcl_script> -tclargs  <options?> <arguments>
```

**PlanAhead batch mode**
```bash
  planAhead -mode batch -notrace -source <tcl_script> -tclargs  <options?> <arguments>
```

**Quartus Software Scripting**
```bash
  quartus_sh -t <tcl_script>  <options?> <arguments>
```

**Tcl console (Vivado/Quartus/PlanAhead/tclsh)**
```tcl
  set argv [list <options?> <arguments>]
  source <tcl_script>
```

Run each script with the `-h` option to see the full list of arguments/options and usage example.

```{tip}
Some of these scripts can be executed with `tclsh`. To do this, you need to install a package called "tcllib", available on **yum** and **apt**.
```

:::{note}
For each of the following commands, the `Options` are optional, while the `Arguments` are mandatory.
:::

### check_syntax.tcl

This script checks the code syntax of a Vivado project.

Arguments:

- `<project_name>`: the project name

:::{warning}
This script has not yet been implemented for Quartus or PlanAhead
:::

### check_list_files.tcl
This script checks if the content of list files and of `hog.conf` match the project. It can also be used to update the list files or the `hog.conf` file.

Options:

* `-project <project_name>`: the project name. If not set, the currently open project will be used by the script.
* `-outFile <file name>`: if set, the script Critical Warnings will be logged to the specified file.
* `-log_list <1/0>`: decides if Critical Warnings of list files shall be logged. To be used together with `-outFile`. Default = 1
* `-log_conf <1/0>`: decides if Critical Warnings of hog.conf shall be logged. To be used together with `-outFile`. Default = 1
* `-recreate`: if set will update the list files
* `-recreate_conf`: if set will recreate the `hog.conf` files
* `-force`: if set, forces the overwriting of list files. To be used together with `-recreate` or `-recreate_conf`
* `-pedantic`: if set, script returns error in case of mismatch
* `-ext_path <external path>`: the absolute path for the external libraries

:::{warning}
This script has not yet been implemented for Quartus or PlanAhead
:::

### add_hog_custom_button.tcl
This script adds the [Hog custom buttons](02a-Hog-Buttons) to the Vivado GUI.

:::{warning}
This script does not work for Quartus or PlanAhead.
:::

### compile_modelsimlib.tcl (VIVADO ONLY)

This script compiles the ModelSim libraries needed to simulate Vivado projects with ModelSim. The compiled libraries will be saved into the directory `./SimulationLib`

### compile_questalib.tcl (VIVADO ONLY)

This script compiles the QuestaSim libraries needed to simulate Vivado projects with QuestaSim. The libraries are stored into the directory `./SimulationLib`.

### compile_riviera.tcl (VIVADO ONLY)

This script compiles the Aldec Rivera libraries needed to simulate Vivado projects with Riviera Pro. The libraries are stored into the directory `./SimulationLib`.

### get_ips.tcl (VIVADO ONLY)

To speed-up IPs re-generation, Hog allows the user to store compiled IPs into an EOS directory and retrieve them instead of recompile them.
This is particularly useful for the CI or if the project repository has been freshly cloned. The IPs are stored to EOS together with their SHA, so they are retrieved only if the `.xci` was not modified.
The instructions to store the IPs to EOS are detailed in the section [IP synthesis](04-Usage).
`get_ips.tcl` is used to retrieve IPs from EOS.
To execute this command you need to have [EOS software](https://eos.web.cern.ch/) installed on your machine.

Arguments:

- `<project_name>`: the project name

Options:

* `-eos_ip_path <IP PATH>`: the EOS path where IPs are stored

### make_doxygen.tcl

This script is used to create the doxygen documentation. The doxygen configuration file must be stored into *./doxygen/doxygen.conf*.
If there is no such file, the command will use *./Hog/Templates/doxygen.conf* as doxygen configuration file.
You require a version of Doxygen newer than 1.8.13 installed on your machine, to execute this script

This script is used by [Hog CI](../02-Hog-CI/05-HOG-CI-Results.md)

### check_yaml_ref.tcl

This script checks that the Hog submodule SHA matches the ref in your `.gitlab-ci.yml` file. The `.gitlab-ci.yml` file defines what stages of the Hog Continuous Integration will be run. For more information, please consult the [Hog-CI chapter](../02-Hog-CI/01-CI-Introduction).

If the two SHAs do not match, the script returns an Error, suggests few solutions to fix the problem.

This script is run by default in the pre-synthesis stage.

### copy_xml.tcl
This script copies IPBus XML files (see [IPbus section](07-IPbus)) listed in a Hog list file of a specific project.
It also replaces the version and SHA place holders, if they are present in any of the XML files.
If a working installation of IPbus is on the system and the `gen_ipbus_addr_decode` script is in the PATH and working, copy_xml.tcl is able to check your XML files against the VHDL address decode files and generate them if they are not there of they need to be updated.

Arguments:

* `<project name>`: The project that contains the IPbus XML list file
* `<dest_dir>`: the destination directory

Options:

* `[-generate]`: if this option is set, the address decode VHDL files will be re generated and copied in the location specified in all the the `.ipb` list files

### project_sha.tcl
This script returns the git SHA of the last commit or the version in which a specified project was modified.

Arguments:

* `<project name>`: the name of the project

Options:

* `-version`: if set, the version is returned rather than the git SHA
* `-ext_path`: path to external libraries if any

### generate_yaml.tcl
This script generates a gitlab-ci.yml configuration file for the dynamic CI child pipeline (see [dynamic CI](../02-Hog-CI/06-Dynamic-CI)). It can also be used to create a template non dynamic project yaml file.

Options:

* `-runall`: if set, it will generate a gitlab-ci yml file for all projects in the Top folder, even if it has not been modified with respect to the target branch
* `-static`: if set, it will generate a template non dynamic project YAML file for every project in the Top folder
* `-ext_path`: path to external libraries if any

Outputs:
* `generated-config.yml`: child pipeline yml needed by dynamic CI, if `-static` flag is not set
* `.gitlab-ci.yml`: template project non dynamic YAML file, if `-static` flag is set
* `new_gitlab-ci.yml`: template project non dynamic YAML file, if `-static` flag is set and `.gitlab-ci.yml` already exists in the project

### reformat.tcl

This script formats tcl scripts indentation.

Arguments:

* `<tcl_script>`: the tcl script to format

Options:

* `-tab_width <pad width>`: the tab width to be used to indent the code (default = 2)
