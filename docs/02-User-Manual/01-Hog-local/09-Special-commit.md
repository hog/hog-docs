## Hog git hooks

Hog supports a series of special git hooks serving different purposes. The full list of the special keywords is described in this section.

### Merge Request description keywords

When a Merge Request is created, the *description* field can be filled with the following keywords:

- `MAJOR_VERSION`: indicates that a new major version will be released. After the branch is merged, the new tag will be `v<M+1>.<m>.<p>` (where M = major, m = minor and p = patch).
- `MINOR_VERSION`: indicates that a new minor version will be released. After the branch is merged, the new tag will be `v<M>.<m+1>.<p>` (where M = major, m = minor and p = patch).

```{note} If neither of the two flags is selected, by default the patch number will be increased by 1.
```

### Commit keywords

- `FEATURE`: if a commit message contains the FEATURE keywords plus a description message, the message will be automatically added to the git changelog after the branch has been merged.

- `RESOLVE_WIP`: if a commit message contains this keyword, the Draft status will be removed from the merge request to start the CI pipeline.
