# Hog2022.2 Changelog

- A new CI job is added to the `collect` stage, checking the timing closure of the built projects, and in case of a failed timing, it fails (with allow to failed option), signalling the users the problematic project.
- Simulation properties are now set using the new `sim.conf` file. Please refer to the [Simulation Chapter](../02-User-Manual/01-Hog-local/02b-Simulation.md) for the detailed instructions.
- A Hog version checker is launched every time the official shell scripts are sourced (`Init.sh`, `CreateProject.sh`, `LaunchWorkflow.sh` and `LaunchSimulation.sh`), checking the running version of Hog, and if a newer stable version is available, suggests the users to update.
- A hierarchical resource estimate is included in the artifacts and in the `bin` folder.
- IDE versions must be now specified in the `hog.conf` files.
- Adding option to create GitLab badges for selected projects, showing the resource utilisation, project version and timing closure status. 
- Restyled the dynamic-CI, with the option to add user jobs as in the standard CI. For more details, please have a look at the [Dynamic-CI chapter](../02-User-Manual/02-Hog-CI/06-Dynamic-CI.md)


# Hog2022.1 Changelog

- IP generated outputs can be saved by the CI on the running machine to speed-up the workflow.
- Option to set the VerilogHeader and SystemVerilog file types inside a list file.
- User IP repository are properly tracked by Hog, and a version number is assigned for each used user IP repository in the project.
- Improved automatic GitLab release, with resource utilization table and MR description.
- Option to shrink to size of the automatic messages in the Merge Request page
- New property for tcl scripts inside a list file, to source them.
- Support to IP/BD generated after the creation of the project by tcl scripts (Vivado only).
- New simulation section inside the `hog.conf` file, to setup the simulation property globally or individually for each simulation set in the project.
- Improved support to Quartus.
- Removed support to `proj.tcl` file. Only `hog.conf` can be used to create the projects.

# Hog2021.2 Changelog 

Main new feature:
- New project file hog.conf
- Support for all simulators supported by Vivado (including Riviera-PRO)
- Simulation library path is now retrieved from environmental variable HOG_SIMULATION_LIB_PATH, if this does not exist, path is set to default SimulationLib inside the top repository folder. An option is added to LaunchWorkflow and CreateProject scripts to overwrite this setting, and set the library path to a custom location (-l/-lib).
- MR pipeline is interruptible, if a newer one starts the old one is cancelled.
- Adding option to configure user ip_repo_paths in the project config file (hog.conf)
- New Vivado buttons, to recreate and check list and project file. Launch Hog/Init.sh to install them
- Support for subfolders inside `Top/`
- FPGA property is now called PART
- max threads are now specified in the hog.conf file in the section [parameters] property name MAX_THREADS
- add pre-creation.tcl and post-creation.tcl scripts, to be executed before or after the project creation. To be stored in the project Top folder

Full changelog available at [gitlab.cern.ch/hog/Hog/-/releases/Hog2021.2](https://gitlab.cern.ch/hog/Hog/-/releases/Hog2021.2)

# Hog2021.1 Changelog

- custom files are reset before each stage
- Stages run on a on_success rule
- remove WARNING about pre and post scripts not belonging to utils_1
- Updating copyright, printing the logo
- Allowing separate building branches (HOG_INTERMEDIATE_BRANCH)
- [bugfix] critical warning given for top name missing if there is flavour
- Locked property for IP in list files
- Synthesised IP products in the repository have always the priority to EOS copies
- add HOG_RESET_FILES variable. All the files or pattern listed here will be reset by Hog-CI before starting synthesis
- create wrapper for sigasi CSV
- create make_sigasi_csv.tcl script
- Improvements to pre/post bitstream scripts
- CI is enabled also when Hog is modified
- TagRepository searches now for the greater tag reachable in the branch and not the newest
- create CopyXML shell wrapper
- bugfix: removed hog version from list of fw versions
- (bugfix) Remove -dirty suffix from SHA to fix missing bitfiles from release in case of dirty bin/bit files
- create Execute, ExecuteRet, Git, GitRet functions to handle git commands and shell commands
- GetArtifactsAndRename gets always the artifacts from the last job in the MR pipeline
- Three stages that can be customised by users (user_pre, user_proj, user_post).
- remove submodules from templates
- remove submodules from GetRepoVersions and update its usage everywhere
- new function GetSubmodule returns the submodule name of a given file or path
- MR messages can be configured with the HOG_MR_MSG environmental variable
- Attaching version and timing to the MR
- Note written at collect_artifact stage
