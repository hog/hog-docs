# Developing for Hog

## Become a member of the Hog community
To become a Hog developer, get in contact with one of us (e.g. [Francesco Gonnella](mailto:francesco.gonnella@cern.ch) or [Davide Cieri](mailto:davide.cieri@cern.ch)), such that we can have a quick feedback of your background and your expertise.

## Developing for Hog
As a Hog developer, please follow these instructions:

1. go to [Hog on gitlab](https://gitlab.cern.ch/hog/Hog)
2. check in the issues list that your improvements/features are not already under development.
3. create a new issue by clicking on "New issue" button
   - use a meaningful short name
   - write a comprehensive description of the changes you plan to make
   - the issue must be assigned to you
   - use labels to indicate whether it is a new feature a bug-fix, etc
   - (OPTIONAL) use due date to indicate when you expect to conclude your work
4. open a new merge request
   - starting from your newly created issue
      * expand Merge Request drop-down
      * expand Create Merge Request
      * direct your merge request to `develop` branch
      * branch name: feature/<issue_short_name>
   - click on "Create Merge Request":
      * a new merge request is created
      * the merge request will be marked as Draft
      * a new development branch is created
5. clone the TestFirmware repository
    ```console
    cd path_to_workdir/
    git clone --recursive https://gitlab.cern.ch/hog/test/TestFirmware.git
    ```
6. Create a new branch in the test firmware repository with the same name as the one in the main Hog repository
    ```console
    cd path_to_workdir/TestFirmware/
    git checkout -b feature/<issue_short_name>
    ```
7. Move Hog to your branch
    ```console
    cd path_to_workdir/TestFirmware/Hog/
    git checkout feature/<issue_short_name>
    ```
8. Develop a test for your new feature *before writing your code*
    - eventually modify an existing tests
    - commit your tests
    ```console
    cd path_to_workdir/TestFirmware/
    git commit <test1> <test1> <...> -m "Adding tests for feature/<issue_short_name>: <brief_test_description>"
    ```
9. Develop the code for the new feature (in this order)
   - all code must be documented using doxygen comments in the code!
   - commit your code
    ```console
    cd path_to_workdir/TestFirmware/Hog/
    git commit <file1> <file2> <...> -m "Working on feature/<issue_short_name>: <brief_commit_description>"
    ```
10. Test your code
    - all your new test must succeed
    - all existing test must succeed
    - if the test fails, fix your code and commit it using --amend
    ```console
    cd path_to_workdir/TestFirmware/Hog/
    git commit --amend --no-edit
    ```
11. If your modification has any impact on the user/maintainer part, update the user manual accordingly
12. Push your changes in the Hog repository
    ```console
    cd path_to_workdir/TestFirmware/Hog/
    git push
    ```
13. Push your changes on the TestFirmware repository
    ```console
    cd path_to_workdir/TestFirmware/
    git push --set-upstream origin feature/<issue_short_name>
    ```
14. Remove Draft status from your Merge Request
    - go to [Hog on gitlab](https://gitlab.cern.ch/hog/Hog)
    - navigate to your merge request
    - click on "Mark as Ready" button
15. Drop us a line at [Hog support](mailto:hog@cern.ch) or on the [Mattermost channel](https://mattermost.web.cern.ch/hog/channels/town-square) 
16. Check your Merge Request and address comments

## Documenting the code

All the code written to implement new features or correct bugs must be documented.
The main source of documentation is doxygen and comments in the code.
The doxygen documentation is collected in a dedicated [website](https://hdl-on-git.web.cern.ch/hdl-on-git/test-doxy/index.html)

An example of how to document new functions

```tcl
# @brief Brief description of this method
#
# After an empty line you can add a more detailed description.
# You can even use many lines
#
# @param[in]  param_1  the description of parameter param_1, this parameter is an input to the function
# @param[out] param_2  the description of parameter param_2, this parameter is an output to the function
#
# @returns A description of the returned value
#
proc  Example {param_1 param_1} {
    if {[info commands get_property] != ""} {
        # some Vivado specific comments that will not end up in the documentation
        return "Vivado"

    } elseif {[info commands quartus_command] != ""} {
        # some Vivado specific comments that will not end up in the documentation
        return "Quartus"
    } else {
        # Tcl Shell
        return "DEBUG_propery_value"
    }
}
```
The resulting documentation will be a brief description in the list of available functions:

 ![](./figures/Example_brief.jpg)

Linked to a detailed description:

 ![](./figures/Example_long.jpg)

The same comment style can be used also for bash scripts provided you use functions in your script.

## Contributing to the Manual

This site uses MkDocs to render the Markdown files.
The source is hosted on Gitlab: [Hog](https://gitlab.cern.ch/hog/hog-docs)

To contribute to the user manual please read this section carefully.
You should first clone the repository:
```console
git clone https://gitlab.cern.ch/hog/hog-docs
```
As an alternative you can use the Web IDE directly from the Gitlab website. This allows you to preview the resulting page.\
If you want to do this locally and haven't set up your permissions for local Gitlab yet, follow the instructions [here](https://docs.gitlab.com/ce/ssh/README.html).
Everything you'll need to edit is inside the `docs/` directory.
Sections are represented by subdirectories within `docs/` while the "Introduction" section comes from `index.md`.
You can create further markdown files to add topics to the section.
Any change you make in the repository is propagated to the website, when you push your commits into the `master` branch.

### Markdown

This manual is made in markdown, a simple language for formatting text. If you're not familiar, there is a handy cheatsheet [here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). There are a few special cases for the specific flavor of markdown that Gitlab uses (most notably for newline syntax) that are documented [here](https://docs.gitlab.com/ee/user/markdown.html).

### Continuous integration set-up

CI for this project was set up using the information in the [mkdocs](https://gitlab.cern.ch/authoring/documentation/mkdocs) repository. The generated website is automatically deployed [here](https://hog.readthedocs.io/en/develop/)
