# How to convert an existing project to Hog

Converting an existing project to Hog means: copying the source files into a git repository, adding Hog as a submodule, creating the Hog list files (text files containing the names of your source files), and writing a `hog.conf` file, storing the project's properties. 

## Before starting
We assume that you are starting from a clean repository and you want to convert a Vivado project stored in a local folder.

If you are migrating to Hog but you are not changing repository, you can follow the instructions below ignoring the creation of the new local repository. In this case you might want to work in a new branch of your repository.

Let's suppose your new repository is called `new_repo` with URL `new_repo_url`, your project is named `myproject` and it is currently stored in some local folder `mypath`. If you do not have a new repository, you can go on GitLab and create one.

## Creating the git repository
First of all create a new folder where you will store your firmware, initialise it as a git repository and connect it to the remote repository:

```bash
  mkdir new_repo
  cd new_repo
  git init
  git remote add origin new_repo_url
```

For the moment, we are going to work on the master branch, that is the default one. If you want, you can create a branch and work on that:

```bash
git checkout -b first_branch
git push origin first_branch
```

## Add Hog to your project
Hog repository should be included as a submodule in the root path of your repository.
To do this, type:

```bash
git submodule add ssh://git@gitlab.cern.ch:7999/hog/Hog.git
```

Here we assumed that you want to use the `ssh` protocol. If you want to use `https` enter
```bash
git submodule add https://gitlab.cern.ch/hog/Hog.git
```

If you prefer `krb5`:
```bash
git submodule add https://:@gitlab.cern.ch:8443/hog/Hog.git
```

However, it is a good idea to not include the submodule protocol explicitly, but to let it be inherited from the repository Hog is included into.
To obtain this edit a file called `.gitmodules` with your favourite text editor (say vim):

```bash
vim .gitmodules
```

In that file, you should find a section as in the following. Modify the URL value by replacing it with a relative path. Note that the URL that must be specified relative to the path of your repository:

```
  [submodule "Hog"]
    path = Hog
    url = ../../hog/Hog.git
```

If your repository is stored on gitlab.com the URL specified in your `.gitmodules` should be

```
  url = ../../hog-cern/hog.git
```

Now from your repository:
```bash
git submodule update
```

This should trigger an error if you made a mistake when editing the repository path.

## Copying some templates from Hog and committing
Now that you have Hog locally, you can start setting up your repository.
Hog provides a set of templates that you can use. For example, you can add a `.gitignore` to your repository with the following command:

```bash
cp Hog/Templates/gitignore .gitignore
```

:::{note}
You might need to modify your `.gitignore` file, if you have a more complicated directory structure, especially for the IP and BD files. For example, the Hog template assumes that you store your IPs in `IP/ip_name/ip_name.xci`. In this case, this file would be enough for you. If your IPs are stored in different directories, you can use several `.gitignore` files.
:::

Let's now make our first commit:

```bash
git add .gitignore .gitmodules Hog
git commit -m "Adding Hog to the repository"
git push origin master
```

If you are working in a branch that is not master, please replace the last instruction with:
```bash
git push origin your_branch_name
```

## Early tagging your repository

Hog assumes that at least a version tag of the form **vM.m.p** is present in your repository.
Let's now create the first Hog tag:

```bash
  git tag -m "First version" v0.0.1
  git push --tags
```

The tag message is necessary as it prevents git from creating a lightweight tag.

## Convert your project to Hog

### Automatic conversion

:::{warning}
This feature is currently only supported for Vivado.
:::
If you want to integrate your project(s) within the Hog framework and you do not want to modify your directory structure, you can do it almost automatically.
You have to copy your entire directory content, including the Vivado project, into the new repository:
```bash
cp ../old_repo/* .

```
:::{warning}
If your project contains submodules, they shall not be copied from the original repository, but rather added as submodules. Please check the [submodules](#submodules) section for extra details.
:::

Now, you have to run the `Init.sh` script, by issuing:
```bash
./Hog/Init.sh
```
This script will, among other things, scan for all the (not Hog) Vivado projects within your current directory and (optionally) convert them into Hog projects. It automatically creates a directory called `Top`, containing one subdirectory for every Vivado project that was converted.
If you browse those subdirectories, you will find that they contain a file called `Top/myproject/hog.conf` and another subdirectory called `Top/myproject/list`. The meaning of each is explained in sections [Configuration File](../../02-User-Manual/01-Hog-local/01-conf.md) and [List Files](../../02-User-Manual/01-Hog-local/02-List-files.md).

Your old Vivado project(s) has now outlived its purpose, and you shall delete it.

At this point, you have to commit your project files (e.g. Top directory, source files, IPs[^IPs], etc.) into the repository:
[^IPs]: IP files require special care. Please check the section [IPs](#intellectual-properties-ip)
```bash
git add Top
git add path_1/file_1.vhd
git add path_2/file_2.xdc
git add path_3/file_3.xci
git commit -m "Adding files to project"
```

Remember that you can always keep track of the uncommitted files by running:
```bash
git status
```

:::{warning}
When you copied the entire content of your old directory, you probably copied also many temporary files that you don't want to be stored into your repository. When you add the files to the repository, please be particularly careful and select only the ones that are really needed. Setting proper `.gitignore` rules can help you in this phase.
:::

At this point you should be ready to create your Hog project (stored in the directory `Projects`), by running:

```bash
./Hog/CreateProject.sh myproject
```

### Manual conversion

#### Generating Hog directory structure

You have to generate a directory structure similar to this one:

![](../01-Getting-Started/figures/directory_structure_new.jpg)

A complete description of the meaning of each folder can be found in the [Hog User Manual](../../02-User-Manual/00-what-is-hog)

#### Top folder
Every Hog-handled HDL repository must have a directory called `Top`, which stores the configuration files, necessary to create the HDL projects. Each subdirectory inside `Top` that contains these configuration files - named **project top-directory** - represents an HDL project in the repository. 

You can start by creating your project top-directory:

```bash
mkdir -p Top/myproject
```

Every project top-directory must contain a subdirectory called `list`, where the so-called hog list files are stored. Let's create it:

```bash
mkdir Top/myproject/list
```

A file named `hog.conf` must be in the project top-directory. Hog reads this configuration file to create a project.
This is a recap of what we have learned up to now:

- A `Top` folder must be in the repository
- Inside this folder, there is one subfolder for each project in the repository, called the project top-directory
- Inside each project's top-directory there is 1. a `list` subdirectory containing the list files 2. a `hog.conf` file;

For more advanced Hog features, additional files and folder are located in the `Top` folder, but we do not discuss them now for simplicity.

To create the project's `hog.conf` script, we will start from the template provided in the `Hog/Templates` folder:

```bash
cp Hog/Templates/hog_vivado.conf Top/myproject/hog.conf
```

Different templates are provided for Vivado, PlanAhead and Quartus and are named `hog_vivado.conf`, `hog_ise.conf` and `hog_quartus.conf`

Use your favourite text editor to modify the template configuration file. There are several things you can edit, depending on your needs, for example, the FPGA name or the synthesis or implementation strategies. The template contains comments to guide you through the process.

Now you can commit everything you just did:

```bash
git add Top
git commit -m "Adding Hog Top folder"
```

:::{note}
From version 2021.2, Hog support a complex subfolder structure. For instance, one could create the project top-directory inside a subfolder of `Top`.

```bash
mkdir -p Top/subfolder/myproject
```

In this case, the resulting project name will be `subfolder/myproject`
:::

#### Importing source files to the project

You are now ready to import the files needed to build your project.

:::{tip}
 Hog gives you the possibility to organise the source files in different VHDL libraries (Verilog does not have the concept of library). You can add your source files into several `.src` files in the list directory. Each of these will correspond to a different library with the same name as the `.src` file (excluding the `.src` extension). For simplicity, in this chapter, we will assume the presence of a unique library with the same name as your project.
:::

First of all, copy the files from your local folder into the git repository.
Exception made for some reserved directory (e.g. `Top`, `Projects`, `bin`) you can put your files wherever you like inside your repository.

In this example, we will create a directory named `lib_myproject` where we will store all the source, simulation and constraint files.

```bash
mkdir -p lib_myproject/source lib_myproject/simulation lib_myproject/constraint
cp ../old_repo/source_1.vhd lib_myproject/source
cp ../old_repo/source_2.v lib_myproject/source
...
cp ../old_repo/simulation_1.vhd lib_myproject/simulation
cp ../old_repo/simulation_2.v lib_myproject/simulation
...
cp ../old_repo/constraint_1.xdc lib_myproject/constraint
cp ../old_repo/constraint_2.tcl lib_myproject/constraint
...
```

After having added all the relevant files in your folders, you have to add their path and file names to the appropriate list files. In this example, we will create:

- One source list-file called `Top/myproject/list/myproject.src`, containing the source files of your project
- One simulation list-file called `Top/myproject/list/myproject.sim`, containing the files used in the simulation (e.g. test benches, modules that read/write files, etc.)
- One constraint list-file called `Top/myproject/list/myproject.con`, containing your constraints (.xdc, .tcl, etc.)

:::{note}
The paths of the files inside the list-files are relative to the main folder of the repository.
:::

If you want, you can add comment lines in the list-files starting with a `#` and you can leave empty lines (or lines containing an arbitrary number of spaces). All of these will be ignored by Hog.

At this point, you might want to check that the files are correctly picked up by regenerating the Hog project. Hog will give you an error if a file is not found.

```bash
./Hog/CreateProject.sh myproject
```

You can open the created project in  `Projects/myproject/myproject.xpr` (or `Projects/myproject/myproject.qpf` for Quartus) with the GUI and check that all the files are there. If not, modify the list files and create the project again. When you are satisfied, you can commit your work:

```bash
git add lib_myproject
git add Top/myproject/list/myproject.src
git add Top/myproject/list/myproject.sim
git add Top/myproject/list/myproject.con
git commit -m "Adding source files"
```

#### Submodules
If your project uses source or simulation files hosted in a separate repository you can add that repository as a git submodule.

```bash
git submodule add my_submodule_url
```
You can add your submodules wherever you like in your repository.

In the same fashion as the source files contained in your repository, files taken from a submodule must be added to a Hog list file.

#### Intellectual Properties (IP)

IP files can be stored anywhere in your repository or submodules. However, particular care must be taken to avoid that the files generated by Vivado or Quartus are kept separated and not committed to the repository.
To achieve this, these simple rules must be followed:

- each IP file (`.xci` for Vivado) must be contained in a sub-folder called with the same name as the `.xci` file (extension excluded).
- only the IP file must be committed to the repository (e.g. `.xci` for Vivado, `.ip` for Quartus)
- a `.gitignore` file must be used to tell git to ignore all non-IP files, a template is provided in the `Templates` directory.

Basically for each IP in your project, run:

```bash
  mkdir -p path_to_your_ips/ip_name/
  cp ../old_repo/ip_name.xci path_to_your_ips/ip_name/
```

Then you must add the `.xci` files to the `.src` list file you want. In this case, we will use a separate file called `IP.src`.

:::{note}
 There is no concept of library for the IPs, so we prefer to put them in a separate `.src` file. You can put them in the same list file as your other source files if you wish. Just open `Top/myproject/list/myproject.src` with a text editor and add them there.
:::

As usual, you can check that the files are correctly picked up by regenerating the project.

```bash
./Hog/CreateProject.sh myproject
```

If you are satisfied with the changes, you can commit your work.

```bash
  git add path_to_your_ips
  git add Top/myproject/list/IP.src
  git commit -m "Adding IP Files"
```

If you have `.coe` files for RAM initialisation or analogous files please make sure that you add them to a `.src` list file and that you specify them with a relative path when customising the IP.

## The top module of your project

You can specify the top module with the property `top=top_module_name`. For example, if your top module is called `mytop` and it is stored inside a `mytop.vhd` file, your list file would look like,

```
# This is an example .src file
mytop.vhd top=mytop
...
```

:::{note}
If you don't specify it, Hog will use the default value `top_<project_name>`, as top module for your project.
:::

Hog provides a set of parameters/generics to your top module at the beginning of the synthesis.
You should add the Hog parameters/generics to your top module to store those values in some registers. The complete list of the variables can be found [here](../02-User-Manual/01-Hog-local/03-parameters-generics.md). The [templates](../02-User-Manual/01-Hog-local/05-templates.md) provided in `./Hog/Templates/`, contain all the generic/parameter names.

We have to customise the library names. In our case, there is only one library called `myproject`.

## Code documentation
Hog can be help you to document VHDL code, automatically generating and deploying the documentation.
Hog works with [Doxygen](http://www.doxygen.nl/) version 1.8.13 or later.
If your code already uses Doxygen style comments, you can easily generate Doxygen documentation.
You just have to create a directory named `doxygen` containing the files used to generate the VHDL documentation.
A file named `doxygen.conf` should be in this directory together with all the files needed to generate your Doxygen documentation.
You can copy a template configuration from `Hog/Templates/doxygen.conf` and modify it.
