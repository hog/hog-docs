# How to update Hog to a newer release

This guide will help you to update Hog to the version you want. We assume you already have a repository handled with Hog that is hosted on CERN Gitlab.

On your project's Gitlab website, create a new merge request and a new branch starting from master.

:::{note}
If you are already working on a branch on your HDL repository, you can update Hog within that branch.
(You can also create a new branch locally and open the merge request on the website later.)
:::

Checkout the branch and go into the Hog directory. Now if you want to update Hog to the latest version:

```bash
  cd Hog
  git checkout master
  git pull
```

Now execute a `git describe` to find out what version you got:

```bash
  git describe
```

You should obtain something like `HogYYYY.n` (e.g. `Hog2020.1`) or `vX.Y.Z` (e.g. `v1.2.3`).

If you want to update to a specific version, go to Hog [repository][https://gitlab.cern.ch/hog/Hog/-/releases] and choose what version you want to update to, let's say you pick version `v1.2.3`.

```bash
  cd Hog
  git checkout v1.2.3
```

Now you have to update your `.gitlab-ci.yml` file, used to configure the GitLab continuous integration (CI). If you are not using Gitlab CI you can skip this part.
Go back to your repo and edit the `.gitlab-ci.yml` with your favourite editor, say emacs:

```bash
  cd ..
  emacs .gitlab-ci.yml
```

At the beginning of the file, in the include section you have a ref, similar to this:

```yaml
include:
    - project: 'hog/Hog'
      file: '/hog.yml'
      ref: 'va.b.c'
```
:::{tip}
In older Hog versions, the file used to be called `gitlab-ci.yml`, so if you were using an older version of Hog, please change `gitlab-ci.yml` to `hog.yml`.
:::

Change the ref to the new version you've just pulled:

```yaml
include:
    - project: 'hog/Hog'
      file: '/hog.yml'
      ref: 'vX.Y.Z'
```

Save the file and close the editor.

```{note}
If you are using the dynamic CI, the include section would look like:

```yaml
include:
  - project: 'hog/Hog'
    file: '/hog-dynamic.yml'
    ref: 'vX.Y.Z'
```

Add the modified files (`Hog` submodule and `.gitlab-ci.yml`), commit, and push:

```bash
  git add .gitlab-ci.yml Hog
  git commit -m "Update Hog to vX.Y.Z"
  git push
```

Finally go to your project's GitLab website, open a merge request for your branch (if you have not done already) and merge it.
