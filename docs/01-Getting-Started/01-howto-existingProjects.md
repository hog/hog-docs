<!-- # Getting Started -->

# How to work with an existing Hog-handled repository

This section is intended for a developer that starts working with an existing HDL project that is managed with Hog.

All the instructions below can be executed both on a Linux shell or on git bash on a Windows machine.

:::{tip}
To open a git bash session navigate to the directory where you want to open the bash. Right-click on the folder and select open git bash here.
:::

For all of the following to work, Vivado (or Quartus or PlanAhead) executable must be in your PATH environment variable: i.e. if you type `vivado` the program must run. If you intend to use Modelsim, Questasim, Riviera or another Xilinx-supported simulation software, also the `vsim` executable must be in the PATH: i.e. if you type `vsim` the simulator should start.

## Requirements
This is a list of the requirements:

- Have git (version 2.9.3 or greater) installed and know git basics
- Have Vivado, PlanAhead or Quartus installed and in the PATH
- Optionally have Questasim installed and vsim in the PATH

## Cloning the repository
First of all, you have to clone the repository, let's call it *repo* from now on. Go to the website of the repository, choose the protocol (ssh, git, https, krb5) and copy the clone the link.

```bash
  git clone --recursive <protocol>://gitlab.cern.ch/repo.git
```

Now you have all the source code and scripts you need in the *repo* folder.

:::{note}
You must choose the *protocol* that works for you: ssh, https, or krb5. We used the `--recursive` option to automatically clone all the submodules included. In general, an HDL repository may or may not include other submodules, but the Hog scripts are always included as submodules. If you have cloned the repository without the recursive options (or if that option does not work, we heard that it happens on Windows), you will have to go inside it and initialise the submodules `git submodule init` and update them `git submodule update`.
:::

## Create Vivado/PlanAhead/Quartus projects
To start working you can now create the Vivado/PlanAhead/Quartus project you are interested in, say it's called *project1*.
To do that, go into the repository (`cd repo`) and type:

```bash
  ./Hog/CreateProject.sh project1
```

This will create the Vivado/PlanAhead/Quartus project in the directory `Projects/project1`.
Inside this directory, you will find the Vivado `.xpr` file (or the Quartus `.qpf` or the PlanAhead `.xise` files).

You can now open the project file with the GUI and synthesise/implement the project.

:::{note}
Inside the Quartus GUI, you must always run a full compile process after creating a new project, otherwise the synthesis process will fail. Due to the Quartus structure, only running the full compilation flow will ensure the Hog generics are properly set.
:::

If you do not know the project name, just run `./Hog/CreateProject.sh` and you will get a list of the existing projects present on the repository.

:::{tip}
Alternatively, you can type `cd Top` (the Top folder is always present in a Hog handled HDL repository) and type `ls`: each directory in this path corresponds to a Vivado/PlanAhead/Quartus project in the repository.
:::

To create all the projects in the repository, you can run the Hog initialisation script, like this:
```bash
  ./Hog/Init.sh
```
This script will also, if you wish, compile Modelsim/Questasim/Riviera libraries, add custom [Hog buttons](../02-User-Manual/01-Hog-local/02a-Hog-Buttons.md) to the Vivado GUI, and convert existing HDL projects to Hog. 

## Adding a new file to the project
When you open *project1*, you can **work with the GUI (almost) normally**.

The `CreateProject.sh` script, that you have just run, has integrated Hog's Tcl scripts in the Vivado/PlanAhead/Quartus project. From now on, Hog scripts will run automatically, every time you start the compilation workflow or any step in the workflow. In particular, the pre-synthesis script will interact with your local git repository and integrate its version and git commit SHA into your HDL project.

We said **almost normally** because there is one exception: modifying the Vivado/Quartus/ISE project. This can be modifying some project's property or adding/renaming a project file.

Adding a new file (HDL code, constraint, IP, etc.) to the project using the GUI is not enough. You **must also add** the file name in one of Hog's list files as explained in the following.

Let's now suppose that you want to add a new file to the project and that this file is located in `repo/dir1/` and is called `file1.hdl`.

First of all, the new file (that is unknown to git) must be added to the repository:

```bash
  cd repo
  git add ./dir1/file1.hdl
  git commit -m "add new file file1.hdl"
  git push
```
Now that the file is safely on git, we have to add it to the Hog project, so that if another developer clones the repository, as you did at the beginning of this guide, the file will appear in the project.

:::{note}
Not all the files stored in the git repository are part of a project: there can be non-HDL files, obsolete files that are stored just in case, new files that are not ready to be used. Moreover, some files could be part of a project and not of another. In our example, the repository could contain project2 and project3 that use different subsets of files in the repository.
:::

This is a new file, unknown to Hog for now, and we want it to be included in the project the next time that we run the `CreateProject.sh` script described above. To do this, you must add the file name and path of `file1.vhd` into a Hog list file. The list files are located in `repo/Top/project1/list/`.

### Manually updating the list file
Let's assume that the list file you want to add your file to is `lib1.src`.
Open the file with a text editor and add the file name and path in a new line.

Now that the new file is included in a list file, you can close the project and re-create it by typing `./Hog/CreateProject.sh <project name>` again.

There is also a quicker option to add a new file without recreating the project. You can add the file with the GUI and **also** add the file to a .src list file. If you choose to do this, in Vivado, you have to choose the correct library when adding the file. The library must have the same name as the `.src` file to which you added the source file. In our example, the HDL file was added to a list file called `lib1.src`, so the library that you have to choose is *lib1*. You can select the library in the Vivado GUI from a drop-down menu when you add the file.

This procedure is valid for any kind of source file. If your file is a constraint file, just add it to a .con list file in the list directory, e.g. `repo/Top/project1/list/lib1.con`.

#### Renaming a file already in the project

If you need to rename or move a file, say from `path1/f1.hdl` to `path2/f2.hdl` do so and change the name in the proper list file accordingly.
Do not forget to rename the file on git as well:

```shell
  git mv path1/f1.hdl path2/f2.hdl
  git commit -m "Renamed f1 into f2"
  git push
```

#### Changing a project property
If you change a project property in the GUI, you have to make sure that the change gets propagated to the repository.
To do that, you need to modify the `hog.conf` file of the specific project and add/modify the property.

In general the esiest way to do it is to copy and adapt what Vivado/Quartus/ISE wrote in the Tcl console when you changed the property. In this way you will find out the exact property name and the value that you have assigned to it.

For more details about the `hog.conf` file, please have a look at the (dedicated section)[../02-User-Manual/01-Hog-local/01-conf.md] in Hog manual.

:::{tip}
An easy way to check if you have modified `hog.conf` as you inteded to, is to close the project and re creater it using the `./Hog/CreateProject.sh` script. When you open the project again, you can check if the property you modified is set as you wanted.
:::

#### What can go wrong?
If you do something wrong (e.g. you add a name of a non-existing file, create a list file with an invalid extension, etc.) you will get an error when you run the `CreateProject.sh` script. In this case, read Hog's error message and try to fix it.
If you do something wrong with Vivado library, or if you forgot to update the list files, the error will show at synthesis time because Vivado will not be able to find the component.

### Automatically updating Hog files

:::{warning}
This feature is currently only supported for Vivado.
:::
Hog provides a Tcl script ([check_list_files.tcl](../02-User-Manual/01-Hog-local/06-Hog-utils.md)) to facilitate the handling of the list and `hog.conf` files.

Users can work normally within the Vivado GUI, adding new files to the projects (and selecting the proper library); afterwards, they can automatically update the list and configuration files by means of the [Hog buttons](../02-User-Manual/01-Hog-local/02a-Hog-Buttons.md):
<!-- <img style="float: middle;" width="700" src="../02-User-Manual/01-Hog-local/figures/hog_custom_button.png"> -->
![Hog buttons](../02-User-Manual/01-Hog-local/figures/hog_custom_button.png)

To check if the list files and `hog.conf` file are up to date, you can click on the *CHK* button or run the following command (e.g. from bash console):
```shell
  vivado -mode batch -notrace -source <repository_path>/Hog/Tcl/utils/check_list_files.tcl -tclargs -project <project name>
```

#### Automatically update list files
To update the list files, you can click on the Hog *LIST* button on the Vivado toolbar or run the following command (e.g. from bash console)

```shell
  vivado -mode batch -notrace -source <repository_path>/Hog/Tcl/utils/check_list_files.tcl -tclargs -project <project name> -recreate -force

```

:::{note}
New vhdl files will be added to `repo/Top/<project>/list/<library>.src`.\
New simulation files will be added to `repo/Top/<project>/list/<simulation_set>.sim`.\
New constraint files will be added to `repo/Top/<project>/list/default.con`.\
Other files (verilog, IPs, etc.) will be added to `repo/Top/<project>/list/default.src`.
:::

### Automatically update `hog.conf`
If you change some property in the project, for example the synthesis or implementation strategy or the maximum fanout, you need to update the `hog.conf` file to make sure that the modification is propagated to the repository.
To do that you can either click the *CONF* button on the Vivado toolbar or run the following command (e.g. from bash console):

```shell
  vivado -mode batch -notrace -source <repository_path>/Hog/Tcl/utils/check_list_files.tcl -tclargs -project <project name> -recreate_conf -force

```

## Adding a new IP

If you want to add a new IP core, say it is called **my_ip1**, you must create it in out-of-context mode and save the `.xci` file (and only that) in the repository in a subfolder of your repository.

Similarly in for Quartus, you can add the `.ip` file to your repository. If your target FPGA only supports `.qip` files (e.g. Intel Max10 FPGAs) you must add the folder in which the IP was generated to your project.

:::{note}
You can keep IPs wherever you want in your repository.
:::

For example: `repo/some_folder/my_ip1/my_ip1.xci`, but particular attention should be paid with the `.gitignore` file.

When Vivado synthesises the IPs, it creates plenty of additional files where the .xci file is located. To avoid to commit those file to the repository, a `.gitignore` file is used.

:::{note}
Every file that is not a `.xci` file inside the directory containing the IP must be ignored by git.
:::

This can be specified in a global `.gitignore` file or in several local `.gitignore` files copied in each directory that you use to store IPs.

The `.gitignore` file, provided in Hog templates, assumes you are storing your IPs in a single directory called `IP` and located in the root of your repository.

Once the IP is created and added to the repository, you can add the `.xci` normally to any source list file in the `list` folder of your project.

### IP initialisation coefficient files (.coe, .mif)
If you have a `.coe` or `.mif` file for RAM initialisation, you can store it anywhere in the repository.
Pay attention to specify the path to this file as a **relative path**, when you customise the IP. Additionally, the coefficient file must be also included in a Hog `.src` list file. 

:::{note}
Browsing to the file location using the GUI will cause the absolute path to be loaded in the textbox. Be careful to replace this with a relative path.
:::

The default `.gitignore` will tell git to ignore all files other than the `.xci` in the IP folder. For this reason, to add your coefficient files to the repository, either exclude them in the `.gitignore` or store them in a different directory.

## A couple of things before getting to work
Here you can find a couple of details and suggestions that can be useful when working with Hog-handled repository.

### Commit before starting the workflow
All the Hog scripts handling version control are automatically added to your project: this means that you can create a reproducible and traceable binary file, even when you run locally.
This will happen **only if you commit your local changes before running synthesis**. You don't have to push! Just commit locally, then you can push when you are sure that your work is good enough.
If you don't commit, Hog will alert you with a Critical Warning at the beginning of the synthesis.

### Different list files
As we have explained above, source files taken from different list files will be added to your project in different "libraries": the name of each library is the name of the list file. When working with components coming from different list files, you will need to formally include the libraries and call the component from the library it belongs to. For example, in VHDL:

```vhdl
library lib1;
use lib1.all;

...

  u_1 : entity lib1.a_component_in_lib1
  port map(
    clk  => clk,
    din  => din,
    dout => dout
    );
```

If working within the same library, you can normally use the "work" library.

### Wrapper scripts
Hog provides bash scripts that can be used to simulate and build the project:

```bash
  ./Hog/LaunchSimulation.sh <proj_name>
  ./Hog/LaunchWorkflow.sh <proj_name>
```

These scripts are wrappers for the Tcl scripts contained in `./Hog/Tcl/launchers/` and are described [here](../02-User-Manual/01-Hog-local/04-Usage.md) in more detail.
