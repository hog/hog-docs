# Frequently Asked Questions (FAQs)

### Does Hog support HLS in Vivado?
Our idea is to store in the HDL repository only the files required to build the project. Therefore, for HLS, we suggest keeping the C++ code in another repo, and then commit to the HDL repository the resulting user IPs and the `.xci` file. For more details on how to work with user IPs in Vivado, have a look at the [User IP Chapter](../02-User-Manual/01-Hog-local/12-User-IPs.md).

### Does Hog support IPs and Block Design in Vivado?
Sure! Our suggestion is to commit only the `.xci` or `.bd` files to the repository, and add them to your list files. However, many developers prefer to generate their IPs or Block Designs with tcl commands. In this case, just generate them in the `post-creation.tcl` script. For more details, visit the [IP Chapter](../02-User-Manual/01-Hog-local/11-IPs.md).

### What are the differences between Hog and other similar tools like HDLmake or IPBB?
The most important difference is that Hog automatically checks your repository before launching the synthesis on your machine.
And embeds the git SHA and version of your project into the binary file. This means that it is really difficult to make a mistake and produce a binary file that does not correspond to something that is committed to the repository.

If you touch anything in the Vivado project after having created it with the Hog script (modify a file, change a property, etc.), when you launch the synthesis Hog will find out and give you a Critical Warning.

So every bitfile is certified, even if produced locally. I don't think any other tool around does that.
Another important difference is that you don't need to install anything, really nothing. You just need Vivado and git (that you would already need), that's it. Other tools like HDLmake or IPBB need python, specific versions, specific modules, and when you have several developers, it's painful to get them up to speed. Hog is thought to be fast and simple, clone, run the script to create the project, work with Vivado.

### Does Hog support GitHub actions?
Hog does not support GitHub actions at the moment, but support is foreseen for the coming releases.
