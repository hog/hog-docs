# Create a Hog project starting from a local project

This tutorial will show how to create a hog project starting from an example Vivado project (IBERT). For this tutorial we will use Hog version `Hog2021.2-9`

## Step 1: create an example Vivado project

Let's start our tutorial by switching to a new empty directory:

```bash

mkdir Example_project
cd Example_project
```

Now we open Vivado console and we create an empty RTL project, we leave everything as default and we select `Kintex-7 KC705 Evaluation Platform` in the `Default Part` panel.
Now we go to the IP catalogue and we type `IBERT` in the search bar and we click on `IBERT 7 Series GTX`.

![](./figures/IBERT.png)

We leave the IP configuration as default, and we click on `IP location`. We change the IP directory to be `IP` (we create the new directory on the GUI). 

![](./figures/ip_location.png)

Now we click on `Done` and we skip the "Generate Output Product".
Now we go on `IP Sources` tab, we right click on the IP and we click on `Open IP Example Design`.

![](./figures/open_ip.png)

We can now close the first project (`project_1`).

If we browse the new project, we notice that there are:
- 1 verilog file *ibert_7series_gtx_0_ex/imports/example_ibert_7series_gtx_0.v*
- 1 IP (xci) *IP/ibert_7series_gtx_0/ibert_7series_gtx_0.xci*
- 1 constraint file (xdc) *ibert_7series_gtx_0_ex/imports/example_ibert_7series_gtx_0.xdc*
- 2 txt files *ibert_7series_gtx_0_ex/imports/example_top_verilog.txt* and *ibert_7series_gtx_0_ex/imports/xdc_7ser_gtx.txt*

## Step 2: create and initialise a git repository

It's time to create a new git repository and add our project files on it.
We start by creating a new blank project on CERN gitlab by clicking [here](https://gitlab.cern.ch/projects/new#blank_project). 
![](./figures/gitlab.png)

We select `Example_project` as project name and we click on `Create Project`.

Now we copy the project URL, e.g. *https://gitlab.cern.ch/$USER/example_project.git*, that will be added as our repo URL.
Now, on a shell, we type:

```bash
git init
git remote add origin https://gitlab.cern.ch/$USER/example_project.git
```

We can now add the **source files** (not the project files) on the git repository.

```bash
git add ibert_7series_gtx_0_ex/imports/example_ibert_7series_gtx_0.v
git add IP/ibert_7series_gtx_0/ibert_7series_gtx_0.xci
git add ibert_7series_gtx_0_ex/imports/example_ibert_7series_gtx_0.xdc
git add ibert_7series_gtx_0_ex/imports/example_top_verilog.txt ibert_7series_gtx_0_ex/imports/xdc_7ser_gtx.txt
git commit -m "First commit, imported project files"
```

## Step 3: import Hog submodule and create Hog project

Now that we have a project in a git repository, we can convert it to a Hog project, to benefit from all the Hog functionalities. 

The first step will be to add Hog submodule in the root path of your repository, preferably using its **relative path**.

The relative path can be found from out repository URL:
- our repository is in [https://gitlab.cern.ch/\$USER/Example\_project](https://gitlab.cern.ch/$USER/Example_project)
- hog repository is in [https://gitlab.cern.ch/hog/Hog](https://gitlab.cern.ch/hog/Hog)

so in our case we will have to add the Hog submodule as:

```bash
  git submodule add ../../hog/Hog.git
  cd Hog
  git checkout Hog2021.2-9
  cd ..
```

:::{note}
If you are working on `gitlab.com`, we advice to add the Hog submodule using its `gitlab.com` mirror. 
- [https://gitlab.com/hog-cern/hog](https://gitlab.com/hog-cern/hog)

To add the repository using the relative path, you should then do

```bash
  git submodule add ../../hog-cern/Hog.git
  cd Hog
  git checkout Hog2021.2-9
  cd ..
```
:::

Next step will be to create the text files containing all project properties and list of project files, as required by hog.
We have to create the following files:
- `Top/<project name>/hog.conf` containing all the project properties, such as *PART*, *Synthesis Strategy*...
- `Top/<project name>/list/<library>.src`containing all the source files (VHDL, verilog, IPs, etc.) that will be put in the VHDL library `<library>`
- `Top/<project name>/list/<simulation_set>.sim` containing all the simulation files that will be put in the simulation set `<simulation_set>`
- `Top/<project name>/list/<constraints_name>.con` containing all the constraints files

We can let Hog handle the creation of the list files automatically, by launching `source Hog/Init.sh` (only in Vivado). However, in our case, we will create all the list files manually.

So we start by creating hog.conf:
```bash
mkdir -p Top/Example_project/list
vim Top/Example_project/hog.conf
```
and we fill it with all the properties required by our project. In our case, we left almost everything as default, so we'll only have to set the *PART* property, which is a *main* property, according to [hog manual](../02-User-Manual/01-Hog-local/01-conf.md#main-section-project-variables):

```ini
#vivado
[main]
PART=xc7k325tffg900-2
```

Now we add all the project files to hog list files. We start by adding all the source files in their HDL library. Since we have only IPs and verilog files, that can't be associated to an HDL library, the library name won't be important. So we can add all the files in a single src file called `work.src`

```bash
vim Top/Example_project/list/work.src
```

Here we add all our files apart from the constraints and simulation ones, and we have to specify which one is the top file:
```
ibert_7series_gtx_0_ex/imports/example_ibert_7series_gtx_0.v top=example_ibert_7series_gtx_0
IP/ibert_7series_gtx_0/ibert_7series_gtx_0.xci
ibert_7series_gtx_0_ex/imports/example_top_verilog.txt 
ibert_7series_gtx_0_ex/imports/xdc_7ser_gtx.txt
```

Finally, we add the constraints in a file called `constraints.con`:
```bash
vim Top/Example_project/list/constraints.con
```

```
ibert_7series_gtx_0_ex/imports/example_ibert_7series_gtx_0.xdc
```

We now add all the files we just created on git and we created a new tag called v0.0.1 as required by Hog:
```bash
git add Top/Example_project/hog.conf
git add Top/Example_project/list/work.src
git add Top/Example_project/list/constraints.con
git commit -m "Converted project to hog project"
git tag v0.0.1 -m "First hog tag"
```

We can now create the Hog project:

```bash
./Hog/CreateProject.sh Example_project
```
The new project will be created in `Projects/Example_project/Example_project.xpr`.
We can open it and confront with the old one; we notice that they are identical.

We can finally build the project by running

```bash
./Hog/LaunchWorkflow.sh Example_project
```

## Step 4: git push
Now we push everything on git:

```bash
git checkout -b Test_branch
git push origin Test_branch
git push --tags
```
